<!doctype html>
<html lang="en-US">
<head>
    <?= $this->Html->charset('utf-8'); ?>
    <title><?= $this->fetch('title') ?></title>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
    <meta name="robots" content="noindex,nofollow">
    <?= $this->Html->css(array(
        'Back./assets/plugins/codemirror/addon/fold/foldgutter.css',
        'Back./assets/bootstrap/css/bootstrap.min.css',
        'Back./assets/fonts/material-design-iconic-font/css/material-design-iconic-font.min',
        'Back./assets/font-awesome-4.4.0/css/font-awesome.min',
        'https://use.fontawesome.com/releases/v5.5.0/css/all.css',
        'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css',
        'Back./assets/css/app.min',
        'Back.custom',
        // 'Back.essentials',
        'https://cdnjs.cloudflare.com/ajax/libs/fontawesome-iconpicker/3.2.0/css/fontawesome-iconpicker.min.css'
    )) .
    $this->fetch('cssTop')
    ?>

</head>
<body class="">

<?php if (isset($_adminCourant)): ?>
    <?= $this->Element('Back.Layout/reconnexion_bar') ?>
<?php endif; ?>

<?= $this->Element('Back.Layout/header')  ?>

<?= $this->Element('Back.Layout/aside')  ?>

<div class="pageWrap">

    <div class="pageContent extended">


        <div class="container">
            <?= $this->Element('Back.Layout/title')  ?>
            <?= $this->Flash->render() ?>
            <?= $this->fetch('content') ?>
        </div>

    </div>

</div>

<script>
    // Tooltip
    $('[rel=tooltip]').tooltip({container: 'body'});

    var baseUrl = '<?= $this->Url->build('/', true) ?>';
</script>

<?= $this->Html->script([
    'Back./assets/js/jquery-1.11.3.min',
    'Back./assets/bootstrap/js/bootstrap.min',
    'Back./assets/js/jquery-ui.min',
    'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.js',
    'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.0/locale/fr.js',
    'Back./assets/js/bootstrap-datepicker.min',
    'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js',
    'Back./js/custom',
    'https://cdn.ckeditor.com/ckeditor5/11.1.1/classic/ckeditor.js',
    'https://cdnjs.cloudflare.com/ajax/libs/fontawesome-iconpicker/3.2.0/js/fontawesome-iconpicker.min.js'
]) ?>

<?= $this->fetch('scriptBottom'); ?>

</body>
</html>
