<?php
echo
    $this->Bs->html5('Valorex - Boîte à outils' , '') .

    // HEAD
    $this->Html->css([
        'styles-merged', 'style_new', 'https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'
    ], ['fullBase' => true]) .
    $this->fetch('cssTop') .
    // \HEAD

    // '<style>
    // body { zoom:3; -moz-transform:scale(3); }</style>' .

    // BODY
    $this->Bs->body() .

    // Contenu
    '<div class="content">' .

        // Contenu
        $this->Bs->container() .
            $this->fetch('content') .
    $this->Bs->close(2) .

    $this->fetch('scriptBottom') .
    $this->Bs->js(['custom'], ['fullBase' => true]) .
    $this->Bs->end();
?>
