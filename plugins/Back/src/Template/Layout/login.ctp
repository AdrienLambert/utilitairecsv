<!doctype html>
<html lang="en-US">
<head>
    <?= $this->Html->charset('utf-8'); ?>
    <title><?= $this->fetch('title'); ?></title>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
    <meta name="robots" content="noindex,nofollow">

    <?=
    $this->Html->css(array(
        'Back./assets/bootstrap/css/bootstrap.min',
        'Back./assets/fonts/material-design-iconic-font/css/material-design-iconic-font.min',
        'Back./assets/css/jquery-ui.min',
        'Back./assets/css/select2.min',
        'Back./assets/font-awesome-4.4.0/css/font-awesome.min',
        // 'Back./assets/css/fontello',
        // 'Back./assets/css/chartist.min',
        // 'Back./assets/css/fullcalendar.min.css',
        // 'Back./assets/css/bootstrap-datepicker.min.css',
        'Back./assets/css/app.min',
        // 'Back./assets/css/datatables.min.css',
        'http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700',
        // '//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css',
         'Back.custom',
    )) .
    $this->fetch('cssTop')
    ?>

    <!-- <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.2/summernote.css" rel="stylesheet"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" />
</head>
<body class="page-login">

<?= $this->Flash->render() ?>

<?= $this->fetch('content') ?>

<script>
    var baseUrl = '<?= $this->Url->build('/', true) ?>';
</script>
<?=
$this->Html->script([
    'Back./assets/js/modernizr-2.8.3.min.js',
    '//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js',
    'Back./assets/js/jquery-1.11.3.min',
    'Back./assets/bootstrap/js/bootstrap.min',
    'Back./assets/js/jquery-ui.min',
    'Back./assets/js/select2.min',
    'Back./assets/js/parsley.min',
    'Back./assets/js/throttle-debounce.min',
    'Back./assets/js/jquery.shuffle.min',
    'Back./assets/js/autosize.min',
    // 'Back./assets/js/moment.min.js',
    // 'Back./assets/js/fullcalendar.min.js' ,
    'Back./assets/js/jquery.fullscreen.min',
    '//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js',
    // 'Back./assets/js/bootstrap-datepicker.min',
    'Back./assets/js/_app',
])
?>

<?= $this->fetch('scriptBottom'); ?>
<?= $this->Flash->render() ?>
<div class="visible-xs visible-sm extendedChecker"></div>
</body>
</html>
