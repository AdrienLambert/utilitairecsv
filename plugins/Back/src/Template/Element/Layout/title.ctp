<?php
echo
$this->Bs->row().
	$this->Bs->col('xs12', 'sm6').
	'<h1 class="pageTitle">' .
		'<a href="#" title="#">' . $this->fetch('title') . '</a>'.
	'</h1>'.
	$this->Bs->close() ;

    // Affichage d'éventuels btn d'actions (ex 'Ajouter un utilisateur')
    if(!empty($toolbar)){
        echo
        $this->Bs->col('xs12', 'sm6','lg6','md6') .
            '<ul class="list list-unstyled list-inline text-right" style="margin-top:17px">';
                foreach ($toolbar as $tool) {
                    echo '<li class="nopadding-right">' . $this->Html->link($tool[0], $tool[1], (isset($tool[2]) ? $tool[2] : [])) . '</li>';
                }
        echo
        '</ul>' .
        $this->Bs->close();
    }
echo
$this->Bs->close();
