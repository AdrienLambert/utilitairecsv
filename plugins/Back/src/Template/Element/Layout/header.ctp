<?= $this->Html->script(['https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'], ['block' => 'scriptBottom']) ; ?>
<!--[if lt IE 8]>
  <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<header id="header">
    <h1 class="logo">
        <a href="#" title="#" class="js-nav-toggler">
            <i class="icon icon-close"></i>
        </a>
        Désignations produits
    </h1>
</header>
