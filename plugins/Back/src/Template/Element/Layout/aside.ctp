<aside class="aside">
    <nav class="simpleList asideNavigation">
        <ul>
            <li><?= $this->Html->link(
                '<i class="fas fa-tachometer-alt"></i><span class="hidden-xs"> Dashboard</span>',
                ['controller' => 'Pages', 'action' => 'index'], 
                ['escape' => false]) 
            ?></li>
        </ul>
    </nav>
</aside>
