<?php
echo
$this->Bs->row().
	$this->Bs->col('xs12', 'sm12','lg6','md6').
	'<h1 class="pageTitle" style="margin-left:0px;">' .
		'<a href="#" title="#">' . $this->fetch('_titre') . '</a>'.
	'</h1>'.
	$this->Bs->close();
	if(!empty($toolbar)){
		echo
		$this->Bs->col('xs12', 'sm12','lg6','md6').
			'<ul class="list list-unstyled list-inline text-right" style="margin-top:17px">';
				foreach ($toolbar as $tool) {
					echo '<li>' . $this->Html->link($tool[0], $tool[1], (isset($tool[2]) ? $tool[2] : null)) . '</li>';
				}
		echo
		'</ul>' .
		$this->Bs->close();
	}
echo
$this->Bs->close();
