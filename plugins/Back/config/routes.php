<?php
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

Router::plugin(
    'Back',
    ['path' => '/admin'],
    function (RouteBuilder $routes) {
        $routes->addExtensions(['pdf']);
        $routes->fallbacks(DashedRoute::class);
    }
);
