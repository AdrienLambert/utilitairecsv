<?php
$config = [
    'number' => '<li class="paginate_button"><a href="{{url}}" tabindex="0">{{text}}</a></li>',
    'current' => '<li class="paginate_button active"><a href="{{url}}">{{text}}</a></li>',
    'nextActive' => '<li class="paginate_button next"><a href="{{url}}"><i class="fa fa-angle-right"></i></a></li>',
    'nextDisabled' => '',
    'prevActive' => '<li class="paginate_button prev"><a href="{{url}}"><i class="fa fa-angle-left"></i></a></li>',
    'prevDisabled' => ''
];
