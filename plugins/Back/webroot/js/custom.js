$( document ).ready(function() {
    // Tooltip
    $('.has-tip').tooltip();

    // Datepicker
    $.fn.datepicker.dates['fr'] = {
        days: ["dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi"],
        daysShort: ["dim.", "lun.", "mar.", "mer.", "jeu.", "ven.", "sam."],
        daysMin: ["d", "l", "ma", "me", "j", "v", "s"],
        months: ["janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre"],
        monthsShort: ["janv.", "févr.", "mars", "avril", "mai", "juin", "juil.", "août", "sept.", "oct.", "nov.", "déc."],
        today: "Aujourd'hui",
        monthsTitle: "Mois",
        clear: "Effacer",
        weekStart: 1,
        format: "dd/mm/yyyy"
    };

    $('.datepicker').datepicker({
        language: "fr"
    });

    $('.datetimepicker').datetimepicker({
        format: "DD/MM/YYYY HH:mm"
    });

    // Gère les champs de formulaires de type file
    formFIle();
});

// Gère les champs de formulaires de type file
function formFIle() {
    $('.input-file-theme').on('change', function() {
        $(this).siblings('.info').text($(this)[0].files[0].name);
    });
}
