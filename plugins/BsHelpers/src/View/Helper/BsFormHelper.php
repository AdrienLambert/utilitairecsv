<?php

namespace BsHelpers\View\Helper;

use Cake\Utility\Text;
use Cake\Utility\Hash;
use Cake\Utility\Inflector;
use Cake\View\View;

/**
 * CakePHP Helper to create forms with Bootstrap 3
 *
 * @author Web and Cow
 */
class BsFormHelper extends \Cake\View\Helper\FormHelper
{

    /**
     * BsForm uses the BsHelper so it can use some feature of it
     * BsForm uses the FormHelper
     *
     * @var array
     */
    public $helpers = ['BsHelpers.Bs', 'Form', 'Url'];

    /**
     * Right settings to have 2 columns (label and inputs) with an horizontal form
     * Default value is :  3
     *
     * @var int
     */
    private $__right = 9;

    /**
     * Left settings to have 2 columns (label and inputs) with an horizontal form
     * Default value is :  9
     *
     * @var int
     */
    private $__left = 3;

    /**
     * Device settings (xs, sm, md, lg)
     * Default value is :  md
     *
     * @var string
     */
    private $__device = 'md';

     /**
     * Defines the type of form being created, horizontal form or inline form. Set by BsFormHelper::create()
     *
     * @var string
     */
    protected $_typeForm = 'horizontal';

    /**
     * Additionnal options for inputs from bootstrap
     *
     * @var array
     */
    protected $_bootstrapOptions = ['state', 'help', 'feedback'];

    /**
     * Feedback configuration from bootstrap
     *
     * @var array
     */
    protected $_feedbackOptions = [
        'success' => [
            'text' => '(success)',
            'icon' => 'ok',
        ],
        'warning' => [
            'text' => '(warning)',
            'icon' => 'warning-sign',
        ],
        'error' => [
            'text' => '(error)',
            'icon' => 'remove',
        ],
    ];

    /**
     * Differents templates for addon in an input-group
     *
     * @var array
     */
    protected $_addonTemplates = [
        'text' => '<span class="input-group-addon :class">:text</span>',
        'submit' => '<span class="input-group-btn"><button type="submit" class="btn btn-:state :class" >:text</button></span>',
        'button' => '<span class="input-group-btn"><button type="button" class="btn btn-:state :class" >:text</button></span>',
        'image' => '<span class="input-group-btn"><input type="image" class="btn btn-:state :class" src=":text" /></span>'
    ];

    protected $_bootstrapTemplates = [
        'error' => '{{content}}',
        'errorList' => '{{content}}',
        'errorItem' => '{{text}}<br />',
        'nestingLabel' => '{{hidden}}<label{{attrs}}>{{input}}{{text}}</label>',
        'radioWrapper' => '<div class="radio">{{label}}</div>',
        'checkboxWrapper' => '<div class="checkbox">{{label}}</div>',
        'select' => '<select name="{{name}}"{{attrs}}>{{content}}</select>',
        'formGroup' => '{{label}}{{input}}'
    ];

    protected $_inputsDate = ['date', 'datetime'];

    /**
     * Bind defaults templates for radio and checkboxes
     *
     * @param \Cake\View\View $View The View this helper is being attached to.
     * @param array $config Configuration settings for the helper.
     */
    public function __construct(View $View, array $config = [])
    {
        $this->_defaultConfig['templates'] = Hash::merge($this->_defaultConfig['templates'], $this->_bootstrapTemplates);
        parent::__construct($View, $config);
    }

    /**
     * Return the current value of $_left
     *
     * @return int
     */
    public function getLeft()
    {
		return $this->__left;
	}

    /**
     * Set the value of $left
     *
     * @param int $val Left value
     * @return void
     */
    public function setLeft($val)
    {
		$this->__left = $val;
	}

    /**
     * Return the current value of $_right
     *
     * @return int
     */
    public function getRight()
    {
		return $this->__right;
	}

    /**
     * Set the value of $right
     *
     * @param int $val Right value
     * @return void
     */
    public function setRight($val)
    {
		$this->__right = $val;
	}

    /**
     * Return the current value of $_typeForm
     *
     * @return string
     */
	protected function _getFormType()
    {
		return $this->_typeForm;
	}

    /**
     * Set the value of $_typeForm
     *
     * @param string $val Type of the form
     * @return void
     */
	public function setFormType($val)
    {
		$this->_typeForm = $val;
	}

    /**
     * Set the value of $__device
     *
     * @param string $val New device
     * @return void
     */
    public function setDevice($val)
    {
        $this->__device = $val;
    }

    /**
     * Return the correct class for the left column of the field
     *
     * @return string
     */
    private function __leftClass()
    {
        if ($this->_getFormType() == 'horizontal') {
            return 'control-label col-' . $this->__device . '-' . $this->__left;
        }
        if ($this->_getFormType() == 'inline') {
            return 'sr-only';
        }
        return 'control-label';
    }

    /**
     * Return the correct class for the right column of the field
     *
     * @param bool $label If the field have a label
     * @return string
     */
    private function __rightClass($label = false)
    {
        if ($this->_getFormType() == 'horizontal') {
            return (!empty($label)) ? 'col-' . $this->__device . '-' . $this->__right : 'col-' . $this->__device . '-' . $this->__right . ' col-' . $this->__device . '-offset-' . $this->__left;
        }
        return '';
    }

    /**
     * Change options if there are errors for the field
     *
     * @param string $fieldName Name of the field
     * @param array  $options   Options for the input
     * @return array
     */
    private function __errorBootstrap($fieldName, $options) {
        // If the field doesn't have any error,
        // we can stop here
        if (!$this->isFieldError($fieldName)) {
            return $options;
        }

        // When the display of boostrap errors is disabled,
        // we can stop here
        if (isset($options['errorBootstrap']) && $options['errorBootstrap'] === false) {
            unset($options['errorBootstrap']);
            return $options;
        }

        // If not, we build the error message
        $options['feedback'] = true;
        $options['errorMessage'] = false;
        $options['state'] = 'error';
        $options['help'] = $this->error($fieldName);
        unset($options['errorBootstrap']);
        return $options;
    }

    /**
     * Build the template for the label
     *
     * @param array $options Array of options
     * @return string
     */
    private function __buildLabel(array $options)
    {
        // If the label is not changed or is not an array
        // Just return the simple format
        if (!isset($options['label']) || !is_array($options['label'])) {
            return [
                'options' => $options,
                'label' => '<label class="' . $this->__leftClass($options) . '"{{attrs}}>{{text}}</label>'
            ];
        }

        // If not, add additionnal classes and change the text for the render
        $text = (isset($options['label']['text'])) ? $options['label']['text'] : '{{text}}';
        $class = (isset($options['label']['class'])) ? $this->__leftClass() . ' ' . $options['label']['class'] : $this->__leftClass();

        unset($options['label']);

        return [
            'options' => $options,
            'label' => '<label class="' . $class . '"{{attrs}}>' . $text . '</label>'
        ];
    }

    /**
     * Get the bootstrap template for the form element that calls
     *
     * @param string $fieldName This should be "Modelname.fieldname"
     * @param array $options    Array of options
     * @return array
     */
    private function __addTemplate($fieldName, array $options)
    {
        // Check if a label exist
        // To define or not an offset for the right column
        // And disable or not the label in the template
        $labelExist = (isset($options['label']) && $options['label'] === false) ? false : true;
        // Get the name of the calling function
        $element = debug_backtrace()[1]['function'];
        $templateFunction = '__getTemplate' . ucfirst($element);
        // Build the label from the label option
        $optionsAndLabel = $this->__buildLabel($options);
        // Get errors of the form to build the correct template
        $optionsAndLabel['options'] = $this->__errorBootstrap($fieldName, $optionsAndLabel['options']);
        // Get the template of the form element
        $template = $this->$templateFunction($optionsAndLabel, $labelExist);

        $element = ($element === 'inputGroup') ? 'input' : $element;

        $options = $optionsAndLabel['options'];

        // Disable the label in the template if it doesn't exist
        $template['formGroup'] = ($labelExist) ? '{{label}}{{' . $element . '}}' : '{{' . $element . '}}';
        // Save the template in array options
        $options['templates'] = (!isset($options['templates'])) ? $template : Hash::merge($template, $options['templates']);

        // Remove bootstrap options if they exist
        unset($options['state']);
        unset($options['help']);
        unset($options['feedback']);

        return $options;
    }

    /**
     * Returns an HTML FORM element.
     *
     * @param mixed $model The context for which the form is being defined. Can
     *   be an ORM entity, ORM resultset, or an array of meta data. You can use false or null
     *   to make a model-less form.
     * @param array $options An array of html attributes and options.
     * @return string An formatted opening FORM tag.
     */
	public function create($model = null, array $options = array())
    {
        $options = Hash::merge([
            'class' => 'form-horizontal',
            'role' => 'form'
        ], $options);

        $this->setFormType('horizontal');

        $othersTypes = [
            'form-inline' => 'inline',
            'form-stacked' => 'basic'
        ];

        foreach ($othersTypes as $keyType => $type) {
            if (preg_match('/' . $keyType . '\b/', $options['class'])) {
                $this->setFormType($type);
                break;
            }
        }

		return parent::create($model, $options);
	}

    /**
     * Generates a form input element complete with label and wrapper div
     *
     * Extends of FormHelper::input() so get same options and params
     *
     * ### New Options
     *
     * - `state` - Change the state of the input to 'error', 'warning' or 'success'
     *
     * ### Options by default for Twitter Bootstrap v3
     *
     * - 'div' - Set to 'false'
     * - 'class' - Add 'form-control'
     * - 'label' - Add a 'control-label' class and if the form is horizontal, put the label into the left column of it
     *
     * ### Options can be extends with no conflicts when the input is being created
     *
     * - 'class'
     * - 'label' - string and array
     *
     * @param string $fieldName This should be "Modelname.fieldname"
     * @param array $options Each type of input takes different options.
     * @return string Completed form widget.
     */
    public function input($fieldName, array $options = [])
    {
        $basicOptions = [
            'class' => ''
        ];
        $options = $this->__addTemplate($fieldName, $options);
        $options = Hash::merge($basicOptions, $options);

        // ----- Load Jasny if data-mask is needed ----- \\
        if (isset($options['data-mask'])) {
            $this->Bs->load('jasny');
        }

        if (!isset($options['type']) || strtolower($options['type']) != 'file') {
            $options['class'] = 'form-control ' . $options['class'];
        }
        // Identify the type of the input to look for the type 'date'
        if (isset($options['type']) && in_array($options['type'], $this->_inputsDate)) {
            $options['class'] .=  ' input-date';
            $this->_defaultConfig['templates']['select'] = '<select name="{{name}}" class="' . $options['class'] . '"{{attrs}}>{{content}}</select>';
            if ($this->_typeForm === 'basic') {
              $options['templates']['formGroup'] = str_replace('{{input}}', '<div>{{input}}</div>', $options['templates']['formGroup']);
            }
            $options['class'] = (isset($options['class'])) ? 'input-date ' . $options['class'] : 'input-date';
            $this->resetTemplates();
        }

        // ----- Length Detector ----- \\
    		if (isset($options['length-detector-option']) || (isset($options['class']) && is_integer(strpos($options['class'], 'length-detector')))) {
    			$jsOptions = '';
    			$ldClass = 'defaults';
    			if (isset($options['length-detector-option'])) {
    				if (isset($options['length-detector-option']['class'])) {
    					$ldClass = $options['length-detector-option']['class'];
    					unset($options['length-detector-option']['class']);
    				}
    				// Length detector attribute encoded to pass it to JS
    				$jsOptions = json_encode($options['length-detector-option']);
    			}

          $this->Bs->load('lengthDetector');

    			// JS send to the page
    			$this->Bs->loadJS('$(document).ready(function(){$("#'.$fieldName.'.length-detector").attr("data-length-detector-class", "' . $ldClass . '").lengthDetector(' . $jsOptions . ');});', true, ['block' => 'scriptBottom']);
    			unset($options['length-detector-option']);
    		}

        return parent::input($fieldName, $options);
    }

    /**
     *
     *
     * @param  [type] $optionsAndLabel [description]
     * @param  [type] $labelExist      [description]
     * @return [type]                  [description]
     */
    private function __getTemplateInput($optionsAndLabel, $labelExist)
    {
        $state = (isset($optionsAndLabel['options']['state'])) ? ' has-' . $optionsAndLabel['options']['state'] : '';
        $help = (isset($optionsAndLabel['options']['help'])) ? '<span class="help-block">' . $optionsAndLabel['options']['help'] . '</span>' : '';
        $feedback = '';
        $iconFeedback = '';
        if (isset($optionsAndLabel['options']['feedback'])) {
            $feedback = ' has-feedback';

            foreach ($this->_feedbackOptions as $feedbackState => $feedbackOptions) {
                if ($optionsAndLabel['options']['state'] === $feedbackState) {
                    $iconFeedback = '<span class="glyphicon glyphicon-' . $feedbackOptions['icon'] . ' form-control-feedback" aria-hidden="true"></span>
                    <span class="sr-only">' . $feedbackOptions['text'] . '</span>';
                    break;
                }
            }
        }

        $templateInput = Text::insert('<input type="{{type}}" name="{{name}}"{{attrs}}/>:help:iconFeedback', ['help' => $help, 'iconFeedback' => $iconFeedback]);
        $templateTextarea = Text::insert('<textarea name="{{name}}"{{attrs}}>{{value}}</textarea>:help:iconFeedback', ['help' => $help, 'iconFeedback' => $iconFeedback]);
        $templateDate = '{{year}}{{month}}{{day}}{{hour}}{{minute}}{{second}}{{meridian}}';
        $inputContainerError = Text::insert('<div class="form-group:state:feedback">{{content}}</div>', [
            'state' => $state,
            'feedback' => $feedback
        ]);
        $inputContainer = Text::insert('<div class="form-group:state:feedback">{{content}}</div>', [
            'state' => $state,
            'feedback' => $feedback
        ]);

        if ($this->_getFormType() !== 'horizontal') {
            return [
                'label' => $optionsAndLabel['label'],
                'inputContainerError' => $inputContainerError,
                'inputContainer' => $inputContainer,
                'input' => $templateInput,
                'textarea' => $templateTextarea,
                'dateWidget' => $templateDate
            ];
        }

        return [
            'label' => $optionsAndLabel['label'],
            'inputContainerError' => $inputContainerError,
            'inputContainer' => $inputContainer,
            'input' => '<div class="' . $this->__rightClass($labelExist) . '">' . $templateInput . '</div>',
            'textarea' => '<div class="' . $this->__rightClass($labelExist) . '">' . $templateTextarea . '</div>',
            'dateWidget' => '<div class="' . $this->__rightClass($labelExist) . '">' . $templateDate . '</div>'
        ];
    }

    /**
     * Generate a form input element with an addon or button on his side.
     *
     *  ### Addon Options
     *
     * - 'content' - The addon content.
     * - 'side'    - Which side the addon will be. Be default 'left'.
     * - 'class'   - Add HTML class attribute.
     * - 'type'    - Change the type of the addon. Values : 'button', 'submit', 'image'.
     * - 'state'   - Change bootstrap button state. Values : 'default', 'primary', 'secondary', 'warning', 'danger'.
     * - 'src'     - URL of the image, if 'type' = 'image'.
     *
     * @param string $fieldName    Extends of BsFormHelper::input()
     * @param array  $addonOptions Array of options, see above for more informations
     * @param mixed  $options      Extends of BsFormHelper::input() so get same options
     * @return string Input-group de Bootstrap
     */
    public function inputGroup($fieldName, $addonOptions, $options = []) {
        $defaultAddOnOptions = [
            'content' => '',
            'side' => 'left',
            'type' => 'text',
            'class' => '',
            'state' => 'default'
        ];

        $addonOptions = (is_array($addonOptions)) ? $addonOptions : ['content' => $addonOptions];

        $options['addon'] = Hash::merge($defaultAddOnOptions, $addonOptions);
        $options = $this->__addTemplate($fieldName, $options);
        unset($options['addon']);
        $options['class'] = (isset($options['class'])) ? 'form-control ' . $options['class'] : 'form-control';

        return parent::input($fieldName, $options);
    }

    /**
     *
     *
     * @param  [type] $optionsAndLabel [description]
     * @param  [type] $labelExist      [description]
     * @return [type]                  [description]
     */
    private function __getTemplateInputGroup($optionsAndLabel, $labelExist)
    {
        $state = (isset($optionsAndLabel['options']['state'])) ? ' has-' . $optionsAndLabel['options']['state'] : '';
        $help = (isset($optionsAndLabel['options']['help'])) ? '<span class="help-block">' . $optionsAndLabel['options']['help'] . '</span>' : '';
        $feedback = '';
        $iconFeedback = '';
        if (isset($optionsAndLabel['options']['feedback'])) {
            $feedback = ' has-feedback';

            foreach ($this->_feedbackOptions as $feedbackState => $feedbackOptions) {
                if ($optionsAndLabel['options']['state'] === $feedbackState) {
                    $iconFeedback = '<span class="glyphicon glyphicon-' . $feedbackOptions['icon'] . ' form-control-feedback" aria-hidden="true"></span>
                    <span class="sr-only">' . $feedbackOptions['text'] . '</span>';
                    break;
                }
            }
        }

        $addOn = Text::insert($this->_addonTemplates[$optionsAndLabel['options']['addon']['type']], ['text' => $optionsAndLabel['options']['addon']['content'], 'class' => $optionsAndLabel['options']['addon']['class'], 'state' => $optionsAndLabel['options']['addon']['state']]);
        $templateInput = '<input type="{{type}}" name="{{name}}"{{attrs}}/>';
        $templateInput = ($optionsAndLabel['options']['addon']['side'] === 'right') ? $templateInput . $addOn : $addOn . $templateInput;
        $templateInput = Text::insert('<div class="input-group">' . $templateInput . '</div>:help:iconFeedback', ['help' => $help, 'iconFeedback' => $iconFeedback]);

        return [
            'label' => $optionsAndLabel['label'],
            'inputContainer' => Text::insert('<div class="form-group:state:feedback">{{content}}</div>', [
                'state' => $state,
                'feedback' => $feedback
            ]),
            'input' => ($this->_getFormType() !== 'horizontal') ? $templateInput : '<div class="' . $this->__rightClass($labelExist) . '">' . $templateInput . '</div>'
        ];
    }

    /**
     * Creates a submit button element.
     *
     * Extends of FormHelper::submit() so get same options and params
     *
     * ### Options by default for Twitter Bootstrap v3
     *
     * - `div` - Set to 'false'
     * - 'label' - Set to 'false'
     * - 'class' - Set into a green button instead of a default button
     *
     *
     * The UX option is set to true by default
     * set 'ux' => false if you don't want to use it for one form
     * In order to correctly work, you need $faLoad and $bsAddonLoad set to true in the BsHelper
     *
     * @param string $caption The label appearing on the button OR if string contains :// or the
     *  extension .jpg, .jpe, .jpeg, .gif, .png use an image if the extension
     *  exists, AND the first character is /, image is relative to webroot,
     *  OR if the first character is not /, image is relative to webroot/img.
     * @param array $options Array of options. See above.
     * @return string A HTML submit button
     */
    public function submit($caption = null, array $options = [])
    {
        $options = Hash::merge(['class' => 'btn btn-success'], $options);
        $ux = (isset($options['ux']) && !$options['ux']) ? false : true;
        unset($options['ux']);

        return $this->__buildSubmitBefore() . parent::button($caption, $options) . $this->__buildSubmitAfter($ux);
	}

    /**
     * Build the html before the submit
     *
     * @return string
     */
    private function __buildSubmitBefore()
    {
        if ($this->_getFormType() == 'horizontal') {
            return '<div class="form-group">' . '<div class="' . $this->__rightClass(false) . '">';
        }
        return '';
    }

    /**
     * Build the html after the radio
     *
     * @param bool $ux     Check if ux animations are activated
     * @param string $type Type of the submit button
     * @return string
     */
    private function __buildSubmitAfter($ux, $type = null) {
        $out = '';
        // if ($ux) {
            // Generate the spin icon
            // $out .= '<i class="fa fa-spinner fa-spin form-submit-wait text-' . $type . '"></i>';
            // $this->Bs->loadJS(
            //     '+function ($) {
            //         $("#' . $this->_getIdForm() . '").submit(function(){
            //             $("#' . $this->_getIdForm() . ' input[type=\'submit\']").prop("disabled" , true);
            //             $("#' . $this->_getIdForm() . ' .form-submit-wait").css("display", "inline-block");
            //         });
            //     }(jQuery);',
            //     true
            // );
        // }
        if ($this->_getFormType() == 'horizontal') {
            $out .= '</div></div>';
        }
        return $out;
    }

    /**
     * Creates a checkbox input widget.
     *
     * Extends of FormHelper::checkbox() so get same options and params
     *
     * Prefer use this function and not BsFormHelper::input() to create checkbox - better results and automatically adapted to Twitter Bootstrap v3
     *
     * ### New Options:
     *
     * - `inline` - choose if you want inline checkbox - 'false' by default, can take 2 values : 'inline' or 'true'
     * - `help` - Add a message under the checkbox to give more informations
     *            Use this option in an array with the label
     *            Example : input('foo', array(
     *                                          'label' => 'name',
     *                                          'help' => 'informations'
     *                                      )
     *                          );
     *
     * ### Options by default for Twitter Bootstrap v3
     *
     * - 'div' - Set to 'false'
     * - 'label' - Set to 'false'
     * - 'label' - Add a 'control-label' class and if the form is horizontal, put the label into the left column of it
     *
     * ### Options can be extends with no conflicts when the input is being created
     *
     * - 'class'
     * - 'label' - string and array
     *
     * ### In case of multiple checkboxes -> use the Bs3FormHelper::select()
     *
     * Some options are added
     * - 'help' - can be for each, always with an array in parameter
     * - 'hiddenField' - 'false' by default
     *
     * @param string $fieldName Name of a field, like this "Modelname.fieldname"
     * @param array $options Array of HTML attributes.
     *
     * @return string An HTML text input element.
     */
    public function checkbox($fieldName, array $options = []) {
        $basicOptions = [
            'help' => false,
            'state' => false,
            'label' => Inflector::camelize($fieldName),
            'label-class' => '',
        ];
        $options = $this->__errorBootstrap($fieldName, $options);
        foreach ($basicOptions as $opt => $valueOpt) {
            if (isset($options[$opt])) {
                $basicOptions[$opt] = $options[$opt];
                unset($options[$opt]);
            }
        }

        $checkbox = parent::checkbox($fieldName, $options);
        $checkbox .= ($basicOptions['label'] !== false) ? ' ' . $basicOptions['label'] : '';
        $checkbox .= ($basicOptions['help']) ? '<span class="help-block">' . $basicOptions['help'] . '</span>' : '';
        $options['type'] = 'checkbox';
        $label = ['text' => $checkbox];
        if (!empty($basicOptions['label-class'])) {
            $label['class'] = $basicOptions['label-class'];
        }

        return $this->__buildCheckboxBefore($basicOptions['state']) . htmlspecialchars_decode($this->_inputLabel($fieldName, $label, $options)) . $this->__buildCheckboxAfter($basicOptions['state']);
    }

    /**
     * Build the html before the checkbox
     *
     * @param string $validationState State of the checkbox
     * @return string
     */
    private function __buildCheckboxBefore($validationState) {
        $out = '';
        if ($this->_getFormType() == 'horizontal') {
            $out .= '<div class="form-group">' .
            '<div class="' . $this->__rightClass(false) . '">';
        }
        if ($validationState) {
            $out .= '<div class="has-' . $validationState . '">';
        }
        return $out .= '<div class="checkbox">';
    }

    /**
     * Build the html after the checkbox
     *
     * @param string $validationState State of the checkbox
     * @return string
     */
    private function __buildCheckboxAfter($validationState) {
        $out = '</div>';
        if ($validationState) {
            $out .= '</div>';
        }
        if ($this->_getFormType() == 'horizontal') {
            $out .= '</div>' . '</div>';
        }
        return $out;
    }

    /**
     * Creates a set of radio widgets.
     *
     * ### Attributes:
     *
     * - `label` - Create a label element for the radio buttons in the left column
     * - `value` - indicate a value that is should be checked
     * - `disabled` - Set to `true` or `disabled` to disable all the radio buttons.
     * - `help` - Add a message under the radio buttons to give more informations
     * - `inline` - Align all the radio buttons
     *
     * @param string $fieldName Name of a field, like this "Modelname.fieldname"
     * @param array $options Radio button options array (as 'value'=>'Text' pairs or as array('label' => 'text', 'help' => 'informations')
     * @param array $attributes Array of HTML attributes, and special attributes above.
     * @return string Completed radio widget set.
     */
    public function radio($fieldName, $options = [], array $attributes = [])
    {
        $attributes = $attributesRadio = $this->__errorBootstrap($fieldName, $attributes);
        unset($attributesRadio['state']);
        unset($attributesRadio['help']);

        // In case of inline radios change the template
        if (isset($attributes['inline']) && $attributes['inline'] === true) {
            $this->_defaultConfig['templates']['radioWrapper'] = '{{label}}';
            $this->_defaultConfig['templates']['nestingLabel'] = '{{hidden}}<label class="radio-inline" {{attrs}}>{{input}}{{text}}</label>';
            $this->resetTemplates();

            $radio = parent::radio($fieldName, $options, $attributesRadio);

            $this->_defaultConfig['templates']['radioWrapper'] = $this->_bootstrapTemplates['radioWrapper'];
            $this->_defaultConfig['templates']['nestingLabel'] = $this->_bootstrapTemplates['nestingLabel'];
            $this->resetTemplates();
        } else {
            $radio = parent::radio($fieldName, $options, $attributesRadio);
        }

        return $this->__buildRadioBefore($fieldName, $attributes) . $radio . $this->__buildRadioAfter($attributes);
    }

    /**
     * Build the html before the radio
     *
     * @param string $fieldName Name of the field
     * @param array $attributes Attributes of the select
     * @param bool $inline      If radio buttons are inline
     * @return string
     */
    private function __buildRadioBefore($fieldName, $attributes)
    {
        $state = (isset($attributes['state'])) ? ' has-' . $attributes['state'] : '';
        $feedback = (isset($attributes['feedback'])) ? ' has-feedback' : '';

        $out = '';
        if ($this->_getFormType() == 'horizontal') {
            $out .= Text::insert('<div class="form-group:state:feedback">', [
                'state' => $state,
                'feedback' => $feedback
            ]);

            if (isset($attributes['label']) && $attributes['label']) {
                $attributes['type'] = 'radio';
                $out .= $this->_inputLabel($fieldName, array('text' => $attributes['label'], 'class' => $this->__leftClass()), $attributes);
                $out .= '<div class="' . $this->__rightClass(true) . '">';
            } else {
                $out .= '<div class="' . $this->__rightClass(false) . '">';
            }
        }
        if (isset($attributes['state'])) {
            $out .= '<div class="has-' . $attributes['state'] . '">';
        }
        return $out;
    }

    /**
     * Build the html after the radio
     *
     * @param bool $inline      If radio buttons are inline
     * @param array $attributes Attributes of the select
     * @return string
     */
    private function __buildRadioAfter($attributes)
    {
        $out = '';
        //----- [help] attribute
        if (isset($attributes['help'])) {
            $out .= '<span class="help-block">' . $attributes['help'] . '</span>';
        }
        if (isset($attributes['state'])) {
            $out .= '</div>';
        }
        if ($this->_getFormType() == 'horizontal') {
            $out .= '</div></div>';
        }
        return $out;
    }

/**
 * Returns a formatted SELECT element.
 *
 * Extends of FormHelper::select() so get same attributes and params
 *
 *
 * - `inline` - Only when attribute [multiple] is checkbox. Align all the checkboxes
 * - 'label' - Add a label to the select element
 *
 * @param string $fieldName Name attribute of the SELECT
 * @param array $options Array of the OPTION elements (as 'value'=>'Text' pairs or as array('label' => 'text', 'help' => 'informations') in case of multiple checkbox) to be used in the
 *	SELECT element
 * @param array $attributes The HTML attributes of the select element.
 * @return string Formatted SELECT element
 */
    public function select($fieldName, $options = array(), array $attributes = array())
    {
        $out = '';
        $checkboxWrapper = '<div class="checkbox :class">{{label}}</div>';
        $isDate = false;
        $inline = (isset($attributes['inline']) && ($attributes['inline'] === 'inline' || $attributes['inline'] === true)) ? true : false;

        // When the select is a multiple checkbox
        if (isset($attributes['multiple']) && $attributes['multiple'] === 'checkbox') {
            if ($inline) {
                $this->_defaultConfig['templates']['nestingLabel'] = '{{hidden}}<label class="checkbox-inline" {{attrs}}>{{input}}{{text}}</label>';
                $checkboxWrapper = '{{label}}';
            }

            $this->_defaultConfig['templates']['checkboxWrapper'] = Text::insert($checkboxWrapper, [
                'class' => (!isset($attributes['class'])) ? '' : $attributes['class']
            ]);
            $this->resetTemplates();
        } else {
            if (!isset($attributes['class'])) {
                $attributes['class'] = 'form-control';
            } else {
                // If the select is not an input date (multi select)
                if (strpos($attributes['class'], 'input-date') === false) {
                    $attributes['class'] = 'form-control ' . $attributes['class'];
                } else {
                    $isDate = true;
                }
            }
        }

        $attributes = $this->__errorBootstrap($fieldName, $attributes);
        $attributesForSelect = $attributes;
        // Clean
        unset($attributesForSelect['state']);
        unset($attributesForSelect['help']);
        unset($attributesForSelect['label']);

        $select = parent::select($fieldName, $options, $attributesForSelect);

        // Reset template
        $this->_defaultConfig['templates']['nestingLabel'] = $this->_bootstrapTemplates['nestingLabel'];
        $this->_defaultConfig['templates']['checkboxWrapper'] = $this->_bootstrapTemplates['checkboxWrapper'];
        $this->resetTemplates();

        return $this->__buildSelectBefore($fieldName, $attributes) . $select . $this->__buildSelectAfter($attributes);
    }

/**
 * Build the html before the select
 *
 * @return string
 */
	private function __buildSelectBefore($fieldName, $attributes) {
        $out = '<div class="form-group">';
        $labelExist = false;

        //----- [label] attribute
        if (isset($attributes['label'])) {
            $labelExist = true;
            $attributes['type'] = 'text';
            $out .= $this->_inputLabel($fieldName, array('text' => $attributes['label'], 'class' => $this->__leftClass()), $attributes);
        }
        $out .= '<div class="' . $this->__rightClass($labelExist) . '">';
        if (isset($attributes['state'])) {
            $out .= '<div class="has-' . $attributes['state'] . '">';
        }
        return $out;
	}

    /**
     * Build the html after the select
     *
     * @return string
     */
    private function __buildSelectAfter($attributes)
    {
        $out = '';
        //----- [help] attribute
        if (isset($attributes['help'])) {
            $out .= '<span class="help-block">' . $attributes['help'] . '</span>';
        }
        if (isset($attributes['state'])) {
            $out .= '</div>';
        }
        return $out .= '</div></div>';
	  }

    /**
     * Returns an HTML element
     *
     * @param string $text Title
     * @param int $h       The level of the title 1-6
     * @return string      the formatted HTML with a row, columns and the title
     */
    public function title($text, $h = 4)
    {
        return $this->__tagForm('h' . $h, $text);
    }

    /**
     * Returns an HTML element
     *
     * @param string $text  Indications
     * @param string $class a class for the p element
     * @return string       the formatted HTML with a row, columns and the indications in a p
     */
    public function indications($text, $class = '')
    {
        if ('' != $class) {
            return $this->__tagForm('p', $text, array('class' => $class));
        } else {
            return $this->__tagForm('p', $text);
        }
    }

    /**
     * Call the Tag function of the BsHelper in a row and a column like the Form
     *
     * @param string $tag    Tag name.
     * @param string $text   String content that will appear inside the element.
     * @param array $options Additional HTML attributes of the element
     * @return string        The formatted tag element
     */
    private function __tagForm($tag, $text, $options = [])
    {
      return $this->Bs->row() .
                // Use of the _right and _left attributes to define with and offset of the column
                '<div class="' . $this->__rightClass() . '">' .
                  $this->Bs->tag($tag, $text, $options) .
                '</div>' .
             $this->Bs->close();
    }

  /**
   * Replace a classic input with a CkEditor
   *
   * $ckEditorLoad must be set to true in the BsHelper so this feature can work
   *
   * @param string $fieldName 	 Name of a field, like this "Modelname.fieldname"
   * @param array $options 		 Each type of input takes different options.
   * @param array $ckEditorOptions Javascript options for the CKEditor instance
   * @return string An HTML text with a line of Javascript to launch CKEDITOR Script
   */
 	public function ckEditor($fieldName, $options = array(), $ckEditorOptions = array())
  {
		$options['type'] = 'textarea';

		$out = $this->input($fieldName, $options);

		// If there is a point in the fieldName
		if (strpos($fieldName, '.') !== false) {
			$nameForReplace = Inflector::camelize(Inflector::slug($fieldName));
		} else {
			$nameForReplace = $this->_modelForm . Inflector::camelize($fieldName);
		}

		// 3rd party libraries and css
        $this->Bs->load('ckeditor');


        $this->Bs->loadJS('CKEDITOR.replace("' . $fieldName . '", ' . json_encode($ckEditorOptions) . ');', true);
		return $out;
	}

/**
 * Return an html element with chosen attached
 *
 * @param String $fieldName name of the field
 * @param Array $options 	options of the select
 * @param Array $attr 		attributes of the select element (multiple etc...)
 * @param Array $chosenAttr attributes of the chosen js call
 * @return string
 */
	public function chosen($fieldName = 'fieldname', $options = array(), $attr = array(), $chosenAttr = array())
    {
		$class = Inflector::slug($fieldName);

		// Default option for the select
		$defaultAttr = array(
			'label' => 'empty',
			'class' => 'chosen-' . $class ,
			'data-placeholder' => 'Cliquez pour choisir',
			'id' => 'chosen_' . uniqid()
		);

		// Default option for chosen
		$defaultChosenAttr = array(
			'width' => '100%',
			'default_multiple_text' => 'Cliquez pour choisir',
			'default_single_text' => 'Cliquez pour choisir',
			'default_no_result_text' => 'Pas de correspondance pour : ',
		);

		// Chosen attribute encoded to pass it to JS
		$chosenAttr = json_encode(Hash::merge($defaultChosenAttr, $chosenAttr));
		// Merge defaults attributes with new attributes
		$attributes = Hash::merge($defaultAttr, $attr);



		// Chosen select created ->
		$select = $this->select($fieldName, $options, $attributes);



        // 3rd party libraries and css
		$this->Bs->load('chosen');
		// JS send to the page
		$this->Bs->loadJS(
			'$("#' . $attributes['id'] . '").chosen(' . $chosenAttr . ');',true
		);

		return $select;
	}

    /**
     * Closes an HTML form, cleans up values set by FormHelper::create(), and writes hidden
     * input fields where appropriate.
     *
     * Overrides parent method to reset the form alignment and grid size.
     *
     * @param array $secureAttributes Secure attributes which will be passed as HTML attributes
     *   into the hidden input elements generated for the Security Component.
     * @return string A closing FORM tag.
     */
    public function end(array $secureAttributes = [])
    {
        // $this->setLeft(3);
        // $this->setRight(9);
        return $this->Form->end($secureAttributes);
    }
}
