<?php

namespace BsHelpers\Test\TestCase\View\Helper;

use BsHelpers\View\Helper\BsHelper;
use BsHelpers\View\Helper\BsFormHelper;
use Cake\View\View;
use Cake\Routing\Router;
use Cake\TestSuite\TestCase;
use Cake\ORM\Entity;
use Cake\ORM\Table;
use Cake\Network\Request;

/**
 * Test stub.
 */
class Article extends Entity
{
}

/**
 * Test BsFormHelper
 *
 * @author Web and Cow
 */
class BsFormHelperTest extends TestCase
{
	/**
	 * setUp function
	 *
	 * @return void
	 */
	public function setUp()
	{
		parent::setUp();

		$this->View = new View();
		$this->Bs = new BsHelper($this->View);
		$this->BsForm = new BsFormHelper($this->View);

		// $request = new Request('articles/add');
  //       $request->here = '/articles/add';
  //       $request['controller'] = 'articles';
  //       $request['action'] = 'add';
  //       $request->webroot = '';
  //       $request->base = '';
  //       $this->BsForm->Form->Url->request = $this->BsForm->Form->request = $request;
    $this->dateRegex = [
        'daysRegex' => 'preg:/(?:<option value="0?([\d]+)">\\1<\/option>[\r\n]*)*/',
        'monthsRegex' => 'preg:/(?:<option value="[\d]+">[\w]+<\/option>[\r\n]*)*/',
        'yearsRegex' => 'preg:/(?:<option value="([\d]+)">\\1<\/option>[\r\n]*)*/',
        'hoursRegex' => 'preg:/(?:<option value="0?([\d]+)">\\1<\/option>[\r\n]*)*/',
        'minutesRegex' => 'preg:/(?:<option value="([\d]+)">0?\\1<\/option>[\r\n]*)*/',
        'meridianRegex' => 'preg:/(?:<option value="(am|pm)">\\1<\/option>[\r\n]*)*/',
    ];

		$this->article = [
		    'schema' => [
		        'id' => ['type' => 'integer'],
		        'author_id' => ['type' => 'integer', 'null' => true],
		        'title' => ['type' => 'string', 'null' => true],
		        'body' => 'text',
		        'published' => ['type' => 'string', 'length' => 1, 'default' => 'N'],
		        '_constraints' => ['primary' => ['type' => 'primary', 'columns' => ['id']]]
		    ],
		    'required' => [
		        'author_id' => true,
		        'title' => true,
		    ]
		];
		Router::connect('/:controller', ['action' => 'index']);
		Router::connect('/:controller/:action/*');
	}

	public function testSetLeft()
	{
		$result = $this->BsForm->setLeft(3);

		$this->assertEquals(3, $this->BsForm->getLeft());
	}

	public function testSetRight()
	{
		$result = $this->BsForm->setRight(3);

		$this->assertEquals(3, $this->BsForm->getRight());
	}

/**
 * Create function
 * @return void
 */
	public function testCreate()
	{
		/////////////////////
		// WITHOUT OPTIONS //
		/////////////////////
		$result = $this->BsForm->create();

		$expected = [
			['form' => ['action', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'accept-charset' => 'utf-8']],
				['div' => ['style' => 'display:none;']],
					['input' => ['name', 'type' => 'hidden', 'value' => 'POST']],
				'/div',
		];
		$this->assertHtml($expected, $result);

		//////////////////
		// WITH OPTIONS //
		//////////////////

		$result = $this->BsForm->create('Model', ['url' => ['controller' => 'models', 'action' => 'action'], 'role' => 'formulaire', 'class' => 'form-inline', 'enctype' => 'multipart/form-data']);

		$expected = array(
			array('form' => array('action' => '/models/action', 'class' => 'form-inline', 'role' => 'formulaire', 'method' => 'post', 'accept-charset' => 'utf-8', 'enctype' => 'multipart/form-data')),
			array('div' => array('style' => 'display:none;')),
			array('input' => array('name', 'type' => 'hidden', 'value' => 'POST')),
			'/div',
		);
		$this->assertHtml($expected, $result);
	}

/**
 * Input function
 * @return void
 */
	public function testInput()
	{
		extract($this->dateRegex);

		///////////
		// BASIC //
		///////////

		$result = $this->BsForm->input('Name');

		$expected = array(
			array('div' => array('class' => 'form-group')),
			array('label' => array('for', 'class' => 'control-label col-md-' . $this->BsForm->getLeft())), 'Name', '/label',
			array('div' => array('class' => 'col-md-' . $this->BsForm->getRight())),
			array('input' => array('name', 'class' => 'form-control ', 'type', 'id')),
			'/div',
			'/div',
		);
		$this->assertHtml($expected, $result);

		/////////////////////////////////
		// BASIC WITH DIFFERENT DEVICE //
		/////////////////////////////////

		$this->BsForm->setDevice('sm');

		$result = $this->BsForm->input('Name');

		$expected = array(
			array('div' => array('class' => 'form-group')),
			array('label' => array('for', 'class' => 'control-label col-sm-' . $this->BsForm->getLeft())), 'Name', '/label',
			array('div' => array('class' => 'col-sm-' . $this->BsForm->getRight())),
			array('input' => array('name', 'class' => 'form-control ', 'type', 'id')),
			'/div',
			'/div',
		);
		$this->assertHtml($expected, $result);

		$this->BsForm->setDevice('md');

		////////////////
		// WITH LABEL //
		////////////////

		$result = $this->BsForm->input('Name', array('label' => 'Other Name'));

		$expected = array(
			array('div' => array('class' => 'form-group')),
			array('label' => array('for', 'class' => 'control-label col-md-' . $this->BsForm->getLeft())), 'Other Name', '/label',
			array('div' => array('class' => 'col-md-' . $this->BsForm->getRight())),
			array('input' => array('name', 'class' => 'form-control ', 'type', 'id')),
			'/div',
			'/div',
		);
		$this->assertHtml($expected, $result);

		////////////////
		// WITH CLASS //
		////////////////

		$this->BsForm->create('Test', array('class' => 'inline'));
		$this->BsForm->input('Test', ['errorBootstrap' => false]);
		$expected = array(
			array('div' => array('class' => 'form-group')),
			array('label' => array('for', 'class' => 'control-label col-md-' . $this->BsForm->getLeft())),
			'Other Name',
			'/label',
			array('div' => array('class' => 'col-md-' . $this->BsForm->getRight())),
			array('input' => array('name', 'class' => 'form-control ', 'type', 'id')),
			'/div',
			'/div',
		);
		$this->assertHtml($expected, $result);
		$this->BsForm->end();

		/////////////////////
		// WITH INPUT MASK //
		/////////////////////
		$this->BsForm->create();
		$result = $this->BsForm->input('Name', array('data-mask' => '99-99-99-99-99'));

		$expected = array(
			array('div' => array('class' => 'form-group')),
			array('label' => array('for' => 'name', 'class' => 'control-label col-md-' . $this->BsForm->getLeft())), 'Name', '/label',
			array('div' => array('class' => 'col-md-' . $this->BsForm->getRight())),
			array('input' => array('name', 'data-mask' => '99-99-99-99-99', 'class' => 'form-control ', 'type', 'id')),
			'/div',
			'/div',
		);
		$this->assertHtml($expected, $result);

		//////////////////////
		// WITH LABEL ARRAY //
		//////////////////////

		$result = $this->BsForm->input('Name', array('label' => array('text' => 'Other Name', 'class' => 'labelClass')));

		$expected = array(
			array('div' => array('class' => 'form-group')),
			array('label' => array('for', 'class' => 'control-label col-md-' . $this->BsForm->getLeft() . ' labelClass')), 'Other Name', '/label',
			array('div' => array('class' => 'col-md-' . $this->BsForm->getRight())),
			array('input' => array('name', 'class' => 'form-control ', 'type', 'id')),
			'/div',
			'/div',
		);
		$this->assertHtml($expected, $result);

		////////////////////////////////////////////////
		// WITH LABEL ARRAY AND NO CLASS ON THE LABEL //
		////////////////////////////////////////////////

		$result = $this->BsForm->input('Name', array('label' => array('text' => 'Other Name')));

		$expected = array(
			array('div' => array('class' => 'form-group')),
			array('label' => array('for', 'class' => 'control-label col-md-' . $this->BsForm->getLeft())), 'Other Name', '/label',
			array('div' => array('class' => 'col-md-' . $this->BsForm->getRight())),
			array('input' => array('name', 'class' => 'form-control ', 'type', 'id')),
			'/div',
			'/div',
		);
		$this->assertHtml($expected, $result);

		///////////////////
		// WITHOUT LABEL //
		///////////////////

		$result = $this->BsForm->input('Name', array('label' => false));

		$expected = array(
			array('div' => array('class' => 'form-group')),
			array('div' => array('class' => 'col-md-' . $this->BsForm->getRight() . ' col-md-offset-' . $this->BsForm->getLeft())),
			array('input' => array('name', 'class' => 'form-control ', 'type', 'id')),
			'/div',
			'/div',
		);
		$this->assertHtml($expected, $result);

		/////////////////
		// FORM-INLINE //
		/////////////////

		$this->BsForm->create('Model', array('class' => 'form-inline'));
		$result = $this->BsForm->input('Name');
		$this->BsForm->end();
		$this->BsForm->setFormType('horizontal');

		$expected = array(
			array('div' => array('class' => 'form-group')),
			array('label' => array('for', 'class' => 'sr-only')), 'Name', '/label',
			array('input' => array('name', 'class' => 'form-control ', 'type', 'id')),
			'/div',
		);
		$this->assertHtml($expected, $result);

		///////////////
		// TEMPLATES //
		///////////////

		$result = $this->BsForm->input('Name', array('templates' => [
			'inputContainer' => '<div class="classTest">{{content}}</div>',
			'input' => '<span>between</span><input type="{{type}}" name="{{name}}"{{attrs}}/>'
		]));

		$expected = array(
			array('div' => array('class' => 'classTest')),
			array('label' => array('for', 'class' => 'control-label col-md-' . $this->BsForm->getLeft())), 'Name', '/label',
			'<span',
			'between',
			'/span',
			array('input' => array('name', 'class' => 'form-control ', 'type', 'id')),
			'/div',
		);
		$this->assertHtml($expected, $result);

		///////////////
		// TEST HELP //
		///////////////

		$result = $this->BsForm->input('Name', array('help' => 'this is a warning', 'state' => 'warning', 'id' => false, 'class' => 'classTest'));

		$expected = array(
			array('div' => array('class' => 'form-group has-warning')),
			array('label' => array('class' => 'control-label col-md-' . $this->BsForm->getLeft())), 'Name', '/label',
			array('div' => array('class' => 'col-md-' . $this->BsForm->getRight())),
			array('input' => array('name', 'class' => 'form-control classTest', 'type')),
			array('span' => array('class' => 'help-block')),
			'this is a warning',
			'/span',
			'/div',
			'/div',
		);
		$this->assertHtml($expected, $result);

		//////////////////
		// STATE SUCESS //
		//////////////////

		$result = $this->BsForm->input('Name', array('state' => 'success'));

		$expected = array(
			array('div' => array('class' => 'form-group has-success')),
			array('label' => array('for', 'class' => 'control-label col-md-' . $this->BsForm->getLeft())), 'Name', '/label',
			array('div' => array('class' => 'col-md-' . $this->BsForm->getRight())),
			array('input' => array('name', 'class' => 'form-control ', 'type', 'id')),
			'/div',
			'/div',
		);
		$this->assertHtml($expected, $result);

		/////////////////
		// STATE ERROR //
		/////////////////

		$result = $this->BsForm->input('Name', array('state' => 'error'));

		$expected = array(
			array('div' => array('class' => 'form-group has-error')),
			array('label' => array('for', 'class' => 'control-label col-md-' . $this->BsForm->getLeft())), 'Name', '/label',
			array('div' => array('class' => 'col-md-' . $this->BsForm->getRight())),
			array('input' => array('name', 'class' => 'form-control ', 'type', 'id')),
			'/div',
			'/div',
		);
		$this->assertHtml($expected, $result);

		/////////////////////
		// FEEDBACK SUCESS //
		/////////////////////

		$result = $this->BsForm->input('Name', array('state' => 'success', 'feedback' => true));

		$expected = array(
			array('div' => array('class' => 'form-group has-success has-feedback')),
			array('label' => array('for', 'class' => 'control-label col-md-' . $this->BsForm->getLeft())), 'Name', '/label',
			array('div' => array('class' => 'col-md-' . $this->BsForm->getRight())),
			array('input' => array('name', 'class' => 'form-control ', 'type', 'id')),
			array('span' => array('class' => 'glyphicon glyphicon-ok form-control-feedback', 'aria-hidden')),
			'/span',
			array('span' => array('class' => 'sr-only')),
			'(success)',
			'/span',
			'/div',
			'/div',
		);
		$this->assertHtml($expected, $result);
	}

	public function testInputDate()
	{
		////////////////
		// INPUT DATE //
		////////////////
		extract($this->dateRegex);
		$result = $this->BsForm->input('Date', ['type' => 'date']);

		$now = strtotime('now');

		$expected = [
			['div' => ['class' => 'form-group']],
				['label' => ['class' => 'control-label col-md-' . $this->BsForm->getLeft()]],
					'Date',
				'/label',
				['div' => ['class' => 'col-md-' . $this->BsForm->getRight()]],
					['select' => ['name', 'class' => 'form-control  input-date']],
						$yearsRegex,
						['option' => ['value' => date('Y', $now), 'selected' => 'selected']],
						date('Y', $now),
						'/option',
					'*/select',
					['select' => ['name', 'class' => 'form-control  input-date']],
						$monthsRegex,
						['option' => ['value' => date('m', $now), 'selected' => 'selected']],
							date('F', $now),
						'/option',
					'*/select',
					['select' => ['name', 'class' => 'form-control  input-date']],
						$daysRegex,
						['option' => ['value' => date('d', $now), 'selected' => 'selected']],
						date('j', $now),
						'/option',
					'*/select',
				'/div',
			'/div',
		];
		$this->assertHtml($expected, $result);
	}

	public function testInputDateWithClass()
	{
		///////////////////////////
		// INPUT DATE WITH CLASS //
		///////////////////////////
		extract($this->dateRegex);
		$result = $this->BsForm->input('Date', ['type' => 'date', 'class' => 'classTest']);

		$now = strtotime('now');

		$expected = [
			['div' => ['class' => 'form-group']],
				['label' => ['class' => 'control-label col-md-' . $this->BsForm->getLeft()]],
					'Date',
				'/label',
				['div' => ['class' => 'col-md-' . $this->BsForm->getRight()]],
					['select' => ['name', 'class' => 'form-control classTest input-date']],
						$yearsRegex,
						['option' => ['value' => date('Y', $now), 'selected' => 'selected']],
							date('Y', $now),
						'/option',
					'*/select',
					['select' => ['name', 'class' => 'form-control classTest input-date']],
						$monthsRegex,
						['option' => ['value' => date('m', $now), 'selected' => 'selected']],
							date('F', $now),
						'/option',
					'*/select',
					['select' => ['name', 'class' => 'form-control classTest input-date']],
						$daysRegex,
						['option' => ['value' => date('d', $now), 'selected' => 'selected']],
							date('j', $now),
						'/option',
					'*/select',
				'/div',
			'/div',
		];
		$this->assertHtml($expected, $result);
	}

	public function testInputDateBasicForm()
	{
		///////////////////////////
		// INPUT DATE BASIC FORM //
		///////////////////////////
		extract($this->dateRegex);
		$this->BsForm->create('Model', array('class' => ''));
		$result = $this->BsForm->input('Date', array('type' => 'date'));
		$this->BsForm->end();

		$now = strtotime('now');

		$expected = [
			['div' => ['class' => 'form-group']],
				['label' => ['class' => 'control-label']],
					'Date',
				'/label',
				'<div',
					['select' => ['name', 'class' => 'form-control  input-date']],
						$yearsRegex,
						['option' => ['value' => date('Y', $now), 'selected' => 'selected']],
							date('Y', $now),
						'/option',
					'*/select',
					['select' => ['name', 'class' => 'form-control  input-date']],
						$monthsRegex,
						['option' => ['value' => date('m', $now), 'selected' => 'selected']],
							date('F', $now),
						'/option',
					'*/select',
					['select' => ['name', 'class' => 'form-control  input-date']],
						$daysRegex,
						['option' => ['value' => date('d', $now), 'selected' => 'selected']],
							date('j', $now),
						'/option',
					'*/select',
				'/div',
			'/div',
		];
		$this->assertHtml($expected, $result);
	}

	public function testLengthDetector()
	{
		/////////////////////
		// LENGTH DETECTOR //
		/////////////////////

		$result = $this->BsForm->input('Name', array('class' => 'length-detector', 'maxlength' => '140'));

		$expected = array(
			array('div' => array('class' => 'form-group')),
			array('label' => array('for', 'class' => 'control-label col-md-' . $this->BsForm->getLeft())), 'Name', '/label',
			array('div' => array('class' => 'col-md-' . $this->BsForm->getRight())),
			array('input' => array('name', 'class' => 'form-control length-detector', 'maxlength' => '140', 'type', 'id')),
			'/div',
			'/div',
		);
		$this->assertHtml($expected, $result);

		/////////////////////////////
		// LENGTH DETECTOR CONFIGS //
		/////////////////////////////

		$result = $this->BsForm->input('Name', array('class' => 'length-detector', 'maxlength' => '140',
			'length-detector-option' => array(
				'class' => 'title')));

		$expected = array(
			array('div' => array('class' => 'form-group')),
			array('label' => array('for', 'class' => 'control-label col-md-' . $this->BsForm->getLeft())), 'Name', '/label',
			array('div' => array('class' => 'col-md-' . $this->BsForm->getRight())),
			array('input' => array('name', 'class' => 'form-control length-detector', 'maxlength' => '140', 'type', 'id')),
			'/div',
			'/div',
		);
		$this->assertHtml($expected, $result);

		/////////////////////////////
		// LENGTH DETECTOR OPTIONS //
		/////////////////////////////

		$result = $this->BsForm->input('Name', array('class' => 'length-detector', 'maxlength' => '140',
			'length-detector-option' => array(
				'interval' => array(
					array('limitChars' => 20, 'bsClass' => 'success')))));

		$expected = array(
			array('div' => array('class' => 'form-group')),
			array('label' => array('for', 'class' => 'control-label col-md-' . $this->BsForm->getLeft())), 'Name', '/label',
			array('div' => array('class' => 'col-md-' . $this->BsForm->getRight())),
			array('input' => array('name', 'class' => 'form-control length-detector', 'maxlength' => '140', 'type', 'id')),
			'/div',
			'/div',
		);
		$this->assertHtml($expected, $result);
	}

	public function testSimpleInputGroup() {
		$result = $this->BsForm->inputGroup('Test', array('content' => 'Simple'));

		$expected = array(
			array('div' => array('class' => 'form-group')),
			array('label' => array('for', 'class' => 'control-label col-md-' . $this->BsForm->getLeft())), 'Test', '/label',
			array('div' => array('class' => 'col-md-' . $this->BsForm->getRight())),
			array('div' => array('class' => 'input-group')),
			array('span' => array('class' => 'input-group-addon ')),
			'Simple',
			'/span',
			array('input' => array('name', 'type', 'id', 'class' => 'form-control')),
			'/div',
			'/div',
			'/div',
		);
		$this->assertHtml($expected, $result);
	}

	public function testInputGroupWithDifferentsOptions() {
		$result = $this->BsForm->inputGroup('Test', array('content' => 'Simple', 'side' => 'right', 'type' => 'submit', 'state' => 'warning', 'class' => 'classTest'));

		$expected = array(
			array('div' => array('class' => 'form-group')),
			array('label' => array('for', 'class' => 'control-label col-md-' . $this->BsForm->getLeft())), 'Test', '/label',
			array('div' => array('class' => 'col-md-' . $this->BsForm->getRight())),
			array('div' => array('class' => 'input-group')),
			array('input' => array('name', 'class' => 'form-control', 'type', 'id')),
			array('span' => array('class' => 'input-group-btn')),
			array('button' => array('type' => 'submit', 'class' => 'btn btn-warning classTest')),
			'Simple',
			'/button',
			'/span',
			'/div',
			'/div',
			'/div',
		);

		$this->assertHtml($expected, $result);

		$result = $this->BsForm->inputGroup('Test', array('content' => 'Simple', 'class' => 'classTest'));

		$expected = array(
			array('div' => array('class' => 'form-group')),
			array('label' => array('for', 'class' => 'control-label col-md-' . $this->BsForm->getLeft())), 'Test', '/label',
			array('div' => array('class' => 'col-md-' . $this->BsForm->getRight())),
			array('div' => array('class' => 'input-group')),
			array('span' => array('class' => 'input-group-addon classTest')),
			'Simple',
			'/span',
			array('input' => array('name', 'type', 'id', 'class' => 'form-control')),
			'/div',
			'/div',
			'/div',
		);

		$this->assertHtml($expected, $result);

		$result = $this->BsForm->inputGroup('Test', 'easy');

		$expected = array(
			array('div' => array('class' => 'form-group')),
			array('label' => array('for', 'class' => 'control-label col-md-' . $this->BsForm->getLeft())), 'Test', '/label',
			array('div' => array('class' => 'col-md-' . $this->BsForm->getRight())),
			array('div' => array('class' => 'input-group')),
			array('span' => array('class' => 'input-group-addon ')),
			'easy',
			'/span',
			array('input' => array('name', 'type', 'id', 'class' => 'form-control')),
			'/div',
			'/div',
			'/div',
		);
		$this->assertHtml($expected, $result);

		// TEST INPUT GROUP WITH IMAGE
		$result = $this->BsForm->inputGroup('Test', array('type' => 'image', 'content' => 'http://myimage.com'));

		$expected = array(
			array('div' => array('class' => 'form-group')),
			array('label' => array('for', 'class' => 'control-label col-md-' . $this->BsForm->getLeft())), 'Test', '/label',
			array('div' => array('class' => 'col-md-' . $this->BsForm->getRight())),
			array('div' => array('class' => 'input-group')),
			array('span' => array('class' => 'input-group-btn')),
			array('input' => array('type' => 'image', 'class' => 'btn btn-default ', 'src' => 'http://myimage.com')),
			'/span',
			array('input' => array('name', 'type', 'id', 'class' => 'form-control')),
			'/div',
			'/div',
			'/div',
		);

		$this->assertHtml($expected, $result);
	}

	public function testInputError()
	{
		/////////////////////
		// error_bootstrap //
		/////////////////////
		$this->article['errors'] = [
            'title' => ['Message erreur']
        ];
		$this->BsForm->create($this->article);
		$result = $this->BsForm->input('title');

		$expected = [
			['div' => ['class' => 'form-group has-error has-feedback']],
				['label' => ['for', 'class' => 'control-label col-md-' . $this->BsForm->getLeft()]],
					'Title',
				'/label',
				['div' => ['class' => 'col-md-' . $this->BsForm->getRight()]],
					['input' => ['name', 'class' => 'form-control  form-error', 'type', 'id', 'required']],
					['span' => ['class' => 'help-block']],
						'Message erreur',
					'/span',
					['span' => ['class' => 'glyphicon glyphicon-remove form-control-feedback', 'aria-hidden' => 'true']],
					'/span',
					['span' => ['class' => 'sr-only']],
						'(error)',
					'/span',
				'/div',
			'/div',
		];

		$this->assertHtml($expected, $result);
	}

	public function testInputGroupWithLabel()
	{
	  	$result = $this->BsForm->inputGroup('User.test', array('content' => 'Simple'), array('label' => 'Mon Label'));

		$expected = array(
			array("div" => array("class" => "form-group")),
			"label" => array("class" => "control-label col-md-3", "for"),
			"Mon Label",
			"/label",
			array("div" => array("class" => "col-md-9")),
			array("div" => array("class" => "input-group")),
			array("span" => array("class" => "input-group-addon ")),
			'Simple',
			'/span',
			"input" => array(
				"name",
				"type" => "text",
				"id",
				"class" => "form-control"
			),
			"/div",
			"/div",
			"/div"
		);

	  	$this->assertHtml($expected, $result);
	}

	public function testInputGroupWithButtonFullOptions()
	{
	  	$result = $this->BsForm->inputGroup('User.test', array('content' => 'Simple', 'type' => 'button', 'state' => 'warning', 'side' => 'right', 'class' => 'ma-class'), array('label' => 'Mon Label'));

		$expected = array(
			array("div" => array("class" => "form-group")),
			"label" => array("class" => "control-label col-md-3", "for"),
			"Mon Label",
			"/label",
			array("div" => array("class" => "col-md-9")),
			array("div" => array("class" => "input-group")),
			"input" => array(
				"name",
				"type" => "text",
				"id",
				"class" => "form-control"
			),
			array("span" => array("class" => "input-group-btn")),
			"button" => array("type" => "button", "class" => "btn btn-warning ma-class"),
			"Simple",
			"/button",
			"/span",
			"/div",
			"/div",
			"/div"
		);

	  	$this->assertHtml($expected, $result);
	}

	public function testCheckboxBasic()
	{
		Router::connect('/:controller/:action/*');
		//////////////////////////
		// CHECKBOX FORM BASIC  //
		//////////////////////////

		$this->BsForm->create('Model', array('class' => ''));
		$result = $this->BsForm->checkbox('Test');
		$this->BsForm->end();

		$expected = [
			['div' => ['class' => 'checkbox']],
			'<label',
			['input' => ['type' => 'hidden', 'name', 'value']],
			['input' => ['type' => 'checkbox', 'name', 'value']],
			' Test',
			'/label',
			'/div',
		];

		$this->assertHtml($expected, $result);
	}

	public function testCheckboxInline()
	{
		Router::connect('/:controller/:action/*');
		//////////////////////////
		// CHECKBOX FORM INLINE //
		//////////////////////////

		$this->BsForm->create('Model', array('class' => 'form-inline'));
		$result = $this->BsForm->checkbox('Test');
		$this->BsForm->end();

		$expected = [
			['div' => ['class' => 'checkbox']],
			'<label',
			['input' => ['name', 'type' => 'hidden', 'value']],
			['input' => ['type' => 'checkbox', 'name', 'value']],
			' Test',
			'/label',
			'/div',
		];

		$this->assertHtml($expected, $result);
	}

	public function testCheckboxHorizontal()
	{
		Router::connect('/:controller/:action/*');
		/////////////////////////
		// CHECKBOX HORIZONTAL //
		/////////////////////////

		$this->BsForm->create('Model', ['class' => 'form-horizontal']);
		$result = $this->BsForm->checkbox('Test');
		$this->BsForm->end();

		$expected = [
			['div' => ['class' => 'form-group']],
			['div' => ['class' => 'col-md-' . $this->BsForm->getRight() . ' col-md-offset-' . $this->BsForm->getLeft()]],
			['div' => ['class' => 'checkbox']],
			'<label',
			['input' => ['name', 'type' => 'hidden', 'value']],
			['input' => ['name', 'type' => 'checkbox', 'value']],
			' Test',
			'/label',
			'/div',
			'/div',
			'/div',
		];

		$this->assertHtml($expected, $result);
	}

	public function testCheckboxState()
	{
		Router::connect('/:controller/:action/*');
		/////////////////////////
		// CHECKBOX W/ STATE   //
		/////////////////////////

		$this->BsForm->create('Model');
		$result = $this->BsForm->checkbox('Test', ['state' => 'error']);
		$this->BsForm->end();

		$expected = [
			['div' => ['class' => 'form-group']],
			['div' => ['class' => 'col-md-' . $this->BsForm->getRight() . ' col-md-offset-' . $this->BsForm->getLeft()]],
			['div' => ['class' => 'has-error']],
			['div' => ['class' => 'checkbox']],
			'<label',
			['input' => ['name', 'type' => 'hidden', 'value']],
			['input' => ['name', 'type' => 'checkbox', 'value']],
			' Test',
			'/label',
			'/div',
			'/div',
			'/div',
		];
		$this->assertHtml($expected, $result);
	}

	public function testCheckboxFullFeatured()
	{
		///////////////////
		// FULL FEATURED //
		///////////////////

		$result = $this->BsForm->checkbox('Test', [
			'class'       => 'classTest',
			'id'          => 'myId',
			'label'       => 'Label',
			'help'        => 'helpTest',
			'hiddenField' => false,
			'label-class' => 'classLabel',
		]);

		$expected = [
			['div' => ['class' => 'form-group']],
				['div' => ['class' => 'col-md-' . $this->BsForm->getRight() . ' col-md-offset-' . $this->BsForm->getLeft()]],
					['div' => ['class' => 'checkbox']],
						['label' => ['for' => 'myId', 'class' => 'classLabel']],
							['input' => ['type' => 'checkbox', 'name', 'value', 'class' => 'classTest', 'id' => 'myId']],
							' Label',
							['span' => ['class' => 'help-block']],
								'helpTest',
							'/span',
						'/label',
					'/div',
				'/div',
			'/div',
		];

		$this->assertHtml($expected, $result);
	}

	public function testSelectSimple()
	{
		///////////////////
		// SIMPLE SELECT //
		///////////////////
		$selectOptions = [
			'first'  => 'Test1',
			'second' => 'Test2',
		];

		$result = $this->BsForm->select('Test', $selectOptions);

		$expected = [
			['div' => ['class' => 'form-group']],
				['div' => ['class' => 'col-md-' . $this->BsForm->getRight() . ' col-md-offset-' . $this->BsForm->getLeft()]],
					['select' => ['class' => 'form-control', 'name']],
						['option' => ['value' => 'first']],
							'Test1',
						'/option',
						['option' => ['value' => 'second']],
							'Test2',
						'/option',
					'/select',
				'/div',
			'/div',
		];

		$this->assertHtml($expected, $result);
	}

	public function testSelectWithClasses()
	{
		///////////////////////////
		// SIMPLE SELECT W/ CLASS//
		///////////////////////////
		$selectOptions = [
			'first'  => 'Test1',
			'second' => 'Test2',
		];

		$result = $this->BsForm->select('Test', $selectOptions, ['class' => 'testingClass']);

		$expected = [
			['div' => ['class' => 'form-group']],
				['div' => ['class' => 'col-md-' . $this->BsForm->getRight() . ' col-md-offset-' . $this->BsForm->getLeft()]],
					['select' => ['class' => 'form-control testingClass', 'name']],
						['option' => ['value' => 'first']],
							'Test1',
						'/option',
						['option' => ['value' => 'second']],
							'Test2',
						'/option',
					'/select',
				'/div',
			'/div',
		];
		$this->assertHtml($expected, $result);
	}

	public function testSelectHelpAndLabel()
	{
		////////////////////////////////////////////
		// SIMPLE SELECT WITH HELP TEXT AND LABEL //
		////////////////////////////////////////////
		$selectOptions = [
			'first'  => 'Test1',
			'second' => 'Test2',
		];

		$result = $this->BsForm->select('Test', $selectOptions, ['help' => 'Test', 'label' => 'Label']);

		$expected = [
			['div' => ['class' => 'form-group']],
				['label' => ['class' => 'control-label col-md-' . $this->BsForm->getLeft()]],
					'Label',
				'/label',
				['div' => ['class' => 'col-md-' . $this->BsForm->getRight()]],
					['select' => ['class' => 'form-control', 'name']],
						['option' => ['value' => 'first']],
							'Test1',
						'/option',
						['option' => ['value' => 'second']],
							'Test2',
						'/option',
					'/select',
					['span' => ['class' => 'help-block']],
						'Test',
					'/span',
				'/div',
			'/div',
		];

		$this->assertHtml($expected, $result);
	}

	public function testSelectMultiple()
	{
		/////////////////////
		// MULTIPLE SELECT //
		/////////////////////
		$selectOptions = [
			'first'  => 'Test1',
			'second' => 'Test2',
		];

		$result = $this->BsForm->select('Test', $selectOptions, ['multiple']);

		$expected = [
			['div' => ['class' => 'form-group']],
				['div' => ['class' => 'col-md-' . $this->BsForm->getRight() . ' col-md-offset-' . $this->BsForm->getLeft()]],
					['select' => ['class' => 'form-control', 'name', 'multiple']],
						['option' => ['value' => 'first']],
							'Test1',
						'/option',
						['option' => ['value' => 'second']],
							'Test2',
						'/option',
					'/select',
				'/div',
			'/div',
		];

		$this->assertHtml($expected, $result);
	}

	public function testSelectMultipleCheckbox()
	{
		///////////////////////
		// MULTIPLE CHECKBOX //
		///////////////////////
		$selectOptions = [
			'first'  => 'Test1',
			'second' => 'Test2',
		];

		$result = $this->BsForm->select('Test', $selectOptions, ['multiple' => 'checkbox']);

		$expected = [
			['div' => ['class' => 'form-group']],
				['div' => ['class' => 'col-md-' . $this->BsForm->getRight() . ' col-md-offset-' . $this->BsForm->getLeft()]],
					['input' => ['type' => 'hidden', 'name', 'value' => '']],
					['div' => ['class' => 'checkbox ']],
						['label' => ['for']],
							['input' => ['type' => 'checkbox', 'name', 'value' => 'first', 'id']],
							'Test1',
						'/label',
					'/div',
					['div' => ['class' => 'checkbox ']],
						['label' => ['for']],
							['input' => ['type' => 'checkbox', 'name', 'value' => 'second', 'id']],
							'Test2',
						'/label',
					'/div',
				'/div',
			'/div',
		];

		$this->assertHtml($expected, $result);
	}

	public function testSelectMultipleCheckboxInline()
	{
		//////////////////////////////
		// MULTIPLE CHECKBOX INLINE //
		//////////////////////////////
		$selectOptions = [
			'first'  => 'Test1',
			'second' => 'Test2',
		];

		$result = $this->BsForm->select('Test', $selectOptions, ['multiple' => 'checkbox', 'inline' => true]);

		$expected = [
			['div' => ['class' => 'form-group']],
				['div' => ['class' => 'col-md-' . $this->BsForm->getRight() . ' col-md-offset-' . $this->BsForm->getLeft()]],
					['input' => ['type' => 'hidden', 'name', 'value' => '']],
					['label' => ['for', 'class' => 'checkbox-inline']],
						['input' => ['type' => 'checkbox', 'name', 'value' => 'first', 'id']],
						'Test1',
					'/label',
					['label' => ['for', 'class' => 'checkbox-inline']],
						['input' => ['type' => 'checkbox', 'name', 'value' => 'second', 'id']],
						'Test2',
					'/label',
				'/div',
			'/div',
		];

		$this->assertHtml($expected, $result);
	}

	public function testSelectMultipleCheckboxInlineWithClass()
	{
		///////////////////////////////////////
		// MULTIPLE CHECKBOX INLINE W/ CLASS //
		///////////////////////////////////////
		$selectOptions = [
			'first'  => 'Test1',
			'second' => 'Test2',
		];

		$result = $this->BsForm->select('Test', $selectOptions, ['multiple' => 'checkbox', 'class' => 'classic']);

		$expected = [
			['div' => ['class' => 'form-group']],
				['div' => ['class' => 'col-md-' . $this->BsForm->getRight() . ' col-md-offset-' . $this->BsForm->getLeft()]],
					['input' => ['type' => 'hidden', 'name', 'value' => '']],
					['div' => ['class' => 'checkbox classic']],
						['label' => ['for']],
							['input' => ['type' => 'checkbox', 'name', 'value' => 'first', 'id']],
							'Test1',
						'/label',
					'/div',
					['div' => ['class' => 'checkbox classic']],
						['label' => ['for']],
							['input' => ['type' => 'checkbox', 'name', 'value' => 'second', 'id']],
							'Test2',
						'/label',
					'/div',
				'/div',
			'/div',
		];
		$this->assertHtml($expected, $result);
	}

	public function testSelectMultipleCheckboxInlineWithClassEqualLabel()
	{
		////////////////////////////////////////////////////////////
		// MULTIPLE CHECKBOX INLINE WHERE CLASS AND LABEL ARE === //
		////////////////////////////////////////////////////////////
		$selectOptions = [
			'first'  => 'Test1',
			'second' => 'Test2',
		];

		$this->BsForm->create('classic', ['form-inline']);
		$result = $this->BsForm->select('classic', $selectOptions, [
			'multiple' => 'checkbox',
			'class' => 'classic',
			'label' => 'classic',
			'inline' => true,
		]);

		$expected = [
			['div' => ['class' => 'form-group']],
				['label' => ['class']],
					'classic',
				'/label',
				['div' => ['class' => 'col-md-' . $this->BsForm->getRight()]],
					['input' => ['type' => 'hidden', 'name', 'value' => '']],
					['label' => ['for', 'class']],
						['input' => ['type' => 'checkbox', 'name', 'value' => 'first', 'id']],
						'Test1',
					'/label',
					['label' => ['for', 'class']],
						['input' => ['type' => 'checkbox', 'name', 'value' => 'second', 'id']],
						'Test2',
					'/label',
				'/div',
			'/div',
		];
		$this->assertHtml($expected, $result);
	}

	public function testRadioSimple()
	{
		//////////////////
		// SIMPLE RADIO //
		//////////////////
		$radioOptions = [
			'first'  => 'Test1',
			'second' => 'Test2',
		];

		$result = $this->BsForm->radio('Test', $radioOptions);

		$expected = [
			['div' => ['class' => 'form-group']],
				['div' => ['class' => 'col-md-' . $this->BsForm->getRight() . ' col-md-offset-' . $this->BsForm->getLeft()]],
					['input' => ['type' => 'hidden', 'name', 'value' => '']],
					['div' => ['class' => 'radio']],
						['label' => ['for']],
							['input' => ['type' => 'radio', 'name', 'value' => 'first', 'id']],
							'Test1',
						'/label',
					'/div',
					['div' => ['class' => 'radio']],
						['label' => ['for']],
							['input' => ['type' => 'radio', 'name', 'value' => 'second', 'id']],
							'Test2',
						'/label',
					'/div',
				'/div',
			'/div',
		];

		$this->assertHtml($expected, $result);
	}

	public function testRadioState()
	{
		///////////////////////////
		// SIMPLE RADIO W/ STATE //
		///////////////////////////
		$radioOptions = [
			'first'  => 'Test1',
			'second' => 'Test2',
		];

		$result = $this->BsForm->radio('Test', $radioOptions, ['state' => 'error']);

		$expected = [
			['div' => ['class' => 'form-group has-error']],
				['div' => ['class' => 'col-md-' . $this->BsForm->getRight() . ' col-md-offset-' . $this->BsForm->getLeft()]],
					['div' => ['class' => 'has-error']],
						['input' => ['type' => 'hidden', 'name', 'value' => '']],
						['div' => ['class' => 'radio']],
							['label' => ['for']],
								['input' => ['type' => 'radio', 'name', 'value' => 'first', 'id']],
								'Test1',
							'/label',
						'/div',
						['div' => ['class' => 'radio']],
							['label' => ['for']],
								['input' => ['type' => 'radio', 'name', 'value' => 'second', 'id']],
								'Test2',
							'/label',
						'/div',
					'/div',
				'/div',
			'/div',
		];
		$this->assertHtml($expected, $result);
	}

	public function testRadioHelp()
	{
		///////////////////////////
		// SIMPLE RADIO W/ HELP //
		///////////////////////////
		$radioOptions = [
			'first'  => 'Test1',
			'second' => 'Test2',
		];

		$result = $this->BsForm->radio('Test', $radioOptions, ['help' => 'Help text']);

		$expected = [
			['div' => ['class' => 'form-group']],
				['div' => ['class' => 'col-md-' . $this->BsForm->getRight() . ' col-md-offset-' . $this->BsForm->getLeft()]],
					['input' => ['type' => 'hidden', 'name', 'value' => '']],
					['div' => ['class' => 'radio']],
						['label' => ['for']],
							['input' => ['type' => 'radio', 'name', 'value' => 'first', 'id']],
							'Test1',
						'/label',
					'/div',
					['div' => ['class' => 'radio']],
						['label' => ['for']],
							['input' => ['type' => 'radio', 'name', 'value' => 'second', 'id']],
							'Test2',
						'/label',
					'/div',
					['span' => ['class' => 'help-block']],
						'Help text',
					'/span',
				'/div',
			'/div',
		];
		$this->assertHtml($expected, $result);
	}

	public function testRadioWithLabel()
	{
		//////////////////////
		// RADIO WITH LABEL //
		//////////////////////
		$radioOptions = [
			'first'  => 'Test1',
			'second' => 'Test2',
		];

		$result = $this->BsForm->radio('Test', $radioOptions, ['label' => 'My radio buttons']);

		$expected = [
			['div' => ['class' => 'form-group']],
				['label' => ['class' => 'control-label col-md-' . $this->BsForm->getLeft()]],
					'My radio buttons',
				'/label',
				['div' => ['class' => 'col-md-' . $this->BsForm->getRight()]],
					['input' => ['type' => 'hidden', 'name', 'value' => '']],
					['div' => ['class' => 'radio']],
						['label' => ['for']],
							['input' => ['type' => 'radio', 'name', 'value' => 'first', 'id']],
							'Test1',
						'/label',
					'/div',
					['div' => ['class' => 'radio']],
						['label' => ['for']],
							['input' => ['type' => 'radio', 'name', 'value' => 'second', 'id']],
							'Test2',
						'/label',
					'/div',
				'/div',
			'/div',
		];

		$this->assertHtml($expected, $result);
	}

	public function testRadioFormInline()
	{
		///////////////////////
		// RADIO FORM INLINE //
		///////////////////////
		Router::connect('/:controller/:action/*');

		$radioOptions = [
			'first'  => 'Test1',
			'second' => 'Test2',
		];

		$this->BsForm->create('Model', ['class' => 'form-inline']);
		$result = $this->BsForm->radio('Test', $radioOptions);
		$this->BsForm->end();

		$expected = [
			['input' => ['type' => 'hidden', 'name', 'value' => '']],
			['div' => ['class' => 'radio']],
				['label' => ['for']],
					['input' => ['type' => 'radio', 'name', 'value' => 'first', 'id']],
					'Test1',
				'/label',
			'/div',
			['div' => ['class' => 'radio']],
				['label' => ['for']],
					['input' => ['type' => 'radio', 'name', 'value' => 'second', 'id']],
					'Test2',
				'/label',
			'/div',
		];

		$this->assertHtml($expected, $result);
	}

	public function testRadioInline()
	{
		//////////////////
		// RADIO INLINE //
		//////////////////
		Router::connect('/:controller/:action/*');

		$radioOptions = [
			'first'  => 'Test1',
			'second' => 'Test2',
		];

		$this->BsForm->create('Model', ['class' => '']);
		$result = $this->BsForm->radio('Test', $radioOptions, ['inline' => true]);
		$this->BsForm->end();

		$expected = [
			['input' => ['name', 'type' => 'hidden', 'value' => '']],
			['label' => ['class' => 'radio-inline', 'for']],
				['input' => ['id', 'name', 'type' => 'radio', 'value' => 'first']],
				'Test1',
			'/label',
			['label' => ['class' => 'radio-inline', 'for']],
				['input' => ['id', 'name', 'type' => 'radio', 'value' => 'second']],
				'Test2',
			'/label'
		];

		$this->assertHtml($expected, $result);
	}

	public function testSubmitInline()
	{
		///////////////////
		// SUBMIT INLINE //
		///////////////////
		$this->BsForm->create('Model', ['class' => 'form-inline']);
		$result = $this->BsForm->submit();
		$this->BsForm->end();

		$expected = [
			['button' => ['class' => 'btn btn-success', 'type' => 'submit']],
			'/button'
		];

		$this->assertHtml($expected, $result);
	}

	public function testSubmitHorizontal($value='')
	{
		///////////////////////
		// SUBMIT HORIZONTAL //
		///////////////////////
		$this->BsForm->create('Model', ['url' => ['controller' => 'truc', 'action' => 'Action'], 'class' => 'form-horizontal']);
		$result = $this->BsForm->submit();
		$this->BsForm->end();

		$expected = [
			['div' => ['class' => 'form-group']],
				['div' => ['class' => 'col-md-' . $this->BsForm->getRight() . ' col-md-offset-' . $this->BsForm->getLeft()]],
					['button' => ['class' => 'btn btn-success', 'type' => 'submit']],
					'/button',
					// ['i' => ['class' => 'fa fa-spinner fa-spin form-submit-wait text-success']],
					// '/i',
				'/div',
			'/div',
		];

		$this->assertHtml($expected, $result);
	}

	public function testSubmitFullOptions()
	{
		//////////////////
		// FULL OPTIONS //
		//////////////////
		$this->BsForm->create('Model', ['url' => ['controller' => 'truc', 'action' => 'Action'], 'class' => 'form-horizontal']);
		$result = $this->BsForm->submit('Send', ['class' => 'btn btn-warning', 'id' => 'myId']);
		$this->BsForm->end();

		$expected = [
			['div' => ['class' => 'form-group']],
				['div' => ['class' => 'col-md-' . $this->BsForm->getRight() . ' col-md-offset-' . $this->BsForm->getLeft()]],
					['button' => ['class' => 'btn btn-warning', 'id' => 'myId', 'type' => 'submit']],
						'Send',
					'/button',
					// ['i' => ['class' => 'fa fa-spinner fa-spin form-submit-wait text-warning']],
					// '/i',
				'/div',
			'/div',
		];

		$this->assertHtml($expected, $result);
	}

	public function testSubmitOtherOptions()
	{
		///////////////////
		// OTHER OPTIONS //
		///////////////////
		$this->BsForm->create('Model', ['url' => ['controller' => 'truc', 'action' => 'Action'], 'class' => 'form-horizontal']);
		$result = $this->BsForm->submit('Send', ['class' => 'classTest', 'ux' => false]);
		$this->BsForm->end();

		$expected = [
			['div' => ['class' => 'form-group']],
				['div' => ['class' => 'col-md-' . $this->BsForm->getRight() . ' col-md-offset-' . $this->BsForm->getLeft()]],
					['button' => ['class' => 'classTest', 'type' => 'submit']],
						'Send',
					'/button',
				'/div',
			'/div',
		];

		$this->assertHtml($expected, $result);
	}

/**
 * End function
 * @return void
 */
	public function testEndSimple()
	{
		/////////////////////
		// WITHOUT OPTIONS //
		/////////////////////
		$result = $this->BsForm->end();

		$expected = ['/form'];
		$this->assertHtml($expected, $result);
	}

	public function testEndWithOptions()
	{
		//////////////////
		// WITH OPTIONS //
		//////////////////
		Router::connect('/:controller/:action/*');

		$this->BsForm->create('Model', ['url' => ['controller' => 'Trucs', 'action' => 'action'], 'class' => 'form-horizontal']);
		$result = $this->BsForm->end(['data-type' => 'hidden']);

		$expected = ['/form'];
		$this->assertHtml($expected, $result);
	}

/**
 * Title function
 * @return void
 */
	public function testTitle()
	{
		/////////////////////
		// WITHOUT OPTIONS //
		/////////////////////

		$result = $this->BsForm->title('Title');

		$expected = array(
			array('div' => array('class' => 'row')),
			array('div' => array('class' => 'col-md-' . $this->BsForm->getRight() . ' col-md-offset-' . $this->BsForm->getLeft())),
			'<h4',
			'Title',
			'/h4',
			'/div',
			'/div',
		);

		$this->assertHtml($expected, $result);

		/////////////////////////////////
		// WITH A SPECIFIC TITLE LEVEL //
		/////////////////////////////////

		$result = $this->BsForm->title('Title', 2);

		$expected = array(
			array('div' => array('class' => 'row')),
			array('div' => array('class' => 'col-md-' . $this->BsForm->getRight() . ' col-md-offset-' . $this->BsForm->getLeft())),
			'<h2',
			'Title',
			'/h2',
			'/div',
			'/div',
		);

		$this->assertHtml($expected, $result);
	}

/**
 * Indications function
 * @return void
 */
	public function testIndications() {

		/////////////////////
		// WITHOUT OPTIONS //
		/////////////////////

		$result = $this->BsForm->indications('indications');

		$expected = array(
			array('div' => array('class' => 'row')),
			array('div' => array('class' => 'col-md-' . $this->BsForm->getRight() . ' col-md-offset-' . $this->BsForm->getLeft())),
			'<p',
			'indications',
			'/p',
			'/div',
			'/div',
		);

		$this->assertHtml($expected, $result);

		//////////////////////////
		// WITH A DEFINED CLASS //
		//////////////////////////

		$result = $this->BsForm->indications('indications', 'class-indications');

		$expected = array(
			array('div' => array('class' => 'row')),
			array('div' => array('class' => 'col-md-' . $this->BsForm->getRight() . ' col-md-offset-' . $this->BsForm->getLeft())),
			array('p' => array('class' => 'class-indications')),
			'indications',
			'/p',
			'/div',
			'/div',
		);

		$this->assertHtml($expected, $result);
	}

/**
 * chosen function
 * @return void
 */
	public function testChosen() {

		/////////////////////
		// WITHOUT OPTIONS //
		/////////////////////
		$tab = array(
			'hello' => 'you',
			'try'   => 'it',
		);

		$field  = 'title';
		$result = $this->BsForm->chosen($field, $tab);

		$expected = [
			['div' => ['class' => 'form-group']],
				['label' => ['for', 'class' => 'control-label col-md-3']],
					'empty',
				'/label',
				['div' => ['class' => 'col-md-9']],
					['select' => ['name' => $field, 'class' => 'form-control chosen-' . $field, 'data-placeholder' => 'Cliquez pour choisir', 'id']],
						['option' => ['value' => 'hello']],
							'you',
						'/option',
						['option' => ['value' => 'try']],
							'it',
						'/option',
					'/select',
				'/div',
			'/div',
		];
		$this->assertHtml($expected, $result);

		////////////////////
		// W/ STATE ERROR //
		////////////////////

		$field  = 'title';
		$result = $this->BsForm->chosen($field, $tab, array('state' => 'error'));

		$expected = [
			['div' => ['class' => 'form-group']],
				['label' => ['for', 'class' => 'control-label col-md-3']],
					'empty',
				'/label',
				['div' => ['class' => 'col-md-9']],
					['div' => ['class' => 'has-error']],
						['select' => ['name' => $field, 'class' => 'form-control chosen-' . $field, 'data-placeholder' => 'Cliquez pour choisir', 'id']],
							['option' => ['value' => 'hello']],
								'you',
							'/option',
							['option' => ['value' => 'try']],
								'it',
							'/option',
						'/select',
					'/div',
				'/div',
			'/div',
		];

		$this->assertHtml($expected, $result);

		////////////////////
		//	 WITH OPTIONS //
		////////////////////

		$tab = array(
			'hello' => 'you',
			'try'   => 'it',
		);

		$field  = 'title';
		$result = $this->BsForm->chosen($field, $tab, array(
			'label'            => 'Ca, ici.',
			'data-placeholder' => 'Cliquez pour selectionner les valeurs recherchées',
		));

		$expected = [
			['div' => ['class' => 'form-group']],
				['label' => ['for', 'class' => 'control-label col-md-3']],
					'Ca, ici.',
				'/label',
				['div' => ['class' => 'col-md-9']],
					['select' => ['name' => $field, 'class' => 'form-control chosen-' . $field, 'data-placeholder' => 'Cliquez pour selectionner les valeurs recherchées', 'id']],
						['option' => ['value' => 'hello']],
							'you',
						'/option',
						['option' => ['value' => 'try']],
							'it',
						'/option',
					'/select',
				'/div',
			'/div',
		];
		$this->assertHtml($expected, $result);
	}

/**
 * Turn an input into a ckeditor input, w/ all options
 *
 * @return [string] Html tags for the input and ckeditor js call
 */
	public function testCkEditor(){
		///////////////////
		//With only field//
		///////////////////
		$result = $this->BsForm->ckEditor('testField');
		$expected = [
			['div' => ['class' => 'form-group']],
				['label' => ['for' => 'testfield', 'class' => 'control-label col-md-' . $this->BsForm->getLeft()]],
					'Test Field',
				'/label',
				['div' => ['class' => 'col-md-' . $this->BsForm->getRight()]],
					['textarea' => ['name', 'class' => 'form-control ', 'rows', 'id' => 'testfield']],
					'/textarea',
				'/div',
			'/div',
		];
		$this->assertHtml($expected, $result);

		////////////////////////
		//With modelname.field//
		////////////////////////

		$result = $this->BsForm->ckEditor('testModel.testField');
		$expected = [
			['div' => ['class']],
				['label' => ['for' => 'testmodel-testfield', 'class' => 'control-label col-md-' . $this->BsForm->getLeft()]],
					'Test Field',
				'/label',
				['div' => ['class' => 'col-md-' . $this->BsForm->getRight()]],
					['textarea' => ['name' => 'testModel[testField]', 'class' => 'form-control ', 'rows', 'id' => 'testmodel-testfield']],
					'/textarea',
				'/div',
			'/div',
		];
		$this->assertHtml($expected, $result);

	}

	/**
	 * tearDown function
	 *
	 * @return void
	 */
	public function tearDown() {
		parent::tearDown();
        unset($this->BsForm, $this->Bs, $this->View);
	}

}
