<?php

namespace BsHelpers\Test\TestCase\View\Helper;

use BsHelpers\View\Helper\BsHelper;
use BsHelpers\View\Helper\BsFormHelper;
use Cake\View\View;
use Cake\Routing\Router;
use Cake\TestSuite\TestCase;

/**
 * Test BsHelper
 *
 * @author Web and Cow
 */
class BsHelperTest extends TestCase
{

	/**
	 * setUp function
	 *
	 * @return void
	 */
    public function setUp()
    {
        parent::setUp();

        $this->View = new View();
        $this->Bs = new BsHelper($this->View);
        $this->BsForm = new BsFormHelper($this->View);
    }

    /**
	 * tearDown function
	 *
	 * @return void
	 */
    public function tearDown()
    {
        parent::tearDown();
        unset($this->Bs, $this->BsForm, $this->View);
    }
    
    /**
     * Test an HTML head template
     *
     * @return void
     */
    public function testHtml()
    {
		/////////////////////
		// WITHOUT OPTIONS //
		/////////////////////

		$result = $this->Bs->html();

		$expected = [
			'<!DOCTYPE html',
			['html' => ['lang' => 'fr']],
			'<head',
			['meta' => ['charset' => 'utf-8']],
			'<title', '/title',
			['meta' => ['name' => 'viewport', 'content' => 'width=device-width, initial-scale=1.0']],
			['meta' => ['name' => 'description', 'content' => '']],
        ];


		$this->assertHtml($expected, $result);

		//////////////////
		// WITH OPTIONS //
		//////////////////

		$result = $this->Bs->html('my title', 'my description', 'en');

		$expected = [
			'<!DOCTYPE html',
			['html' => ['lang' => 'en']],
			'<head',
			['meta' => ['charset' => 'utf-8']],
			'<title', 'my title', '/title',
			['meta' => ['name' => 'viewport', 'content' => 'width=device-width, initial-scale=1.0']],
			['meta' => ['name' => 'description', 'content' => 'my description']],
		];

		$this->assertHtml($expected, $result);
	}
    
	/**
     * Test an HTML5 head template
     *
     * @return void
     */
	public function testHtml5()
	{
		$result = $this->Bs->html5('my title', 'my description', 'en');

		// I don't know actually how to test the html comment return

		$expected = [
			'<!DOCTYPE html',
			['html' => ['lang' => 'en']],
			'<head',
			['meta' => ['charset' => 'utf-8']],
			'<title', 'my title', '/title',
			['meta' => ['name' => 'viewport', 'content' => 'width=device-width, initial-scale=1.0']],
			['meta' => ['name' => 'description', 'content' => 'my description']],
		];

		$this->assertHtml($expected, $result);
	}

	/**
     * Test an HTML body template
     *
     * @return void
     */
	public function testBody()
	{
		///////////////////
		// WITHOUT CLASS //
		///////////////////

		$result = $this->Bs->body();

		$expected = ['/head', '<body'];

		$this->assertHtml($expected, $result);

		////////////////
		// WITH CLASS //
		////////////////

		$result = $this->Bs->body('classTest');

		$expected = ['/head', ['body' => ['class' => 'classTest']]];

		$this->assertHtml($expected, $result);
	}

	/**
     * Test an HTML end body template
     *
     * @return void
     */
	public function testEnd()
	{
		$result = $this->Bs->end();

		$expected = ['/body', '/html'];

		$this->assertHtml($expected, $result);
	}

    public function testCss()
    {
		////////////////
		// BASIC LOAD //
		////////////////

		$result = $this->Bs->css();

		$expected = array(
			array('link' => array('rel' => 'stylesheet', 'href')),
			array('link' => array('rel' => 'stylesheet', 'href')),
			array('link' => array('rel' => 'stylesheet', 'href')),
		);

		$this->assertHtml($expected, $result);
	}

	public function testMoreCss()
	{
		//////////////
		// MORE CSS //
		//////////////

		$result = $this->Bs->css(['myCss.css', 'myOther']);

		$expected = array(
			array('link' => array('rel' => 'stylesheet', 'href')),
			array('link' => array('rel' => 'stylesheet', 'href')),
			array('link' => array('rel' => 'stylesheet', 'href')),
			array('link' => array('rel' => 'stylesheet', 'href' => '/css/myCss.css')),
			array('link' => array('rel' => 'stylesheet', 'href' => '/css/myOther.css')),
		);

		$this->assertHtml($expected, $result);
	}

	public function testJs()
	{
		////////////////
		// BASIC LOAD //
		////////////////

		$result = $this->Bs->js();

		$expected = array(
			array('script' => array('src')), '/script',
			array('script' => array('src')), '/script',
		);

		$this->assertHtml($expected, $result);
	}
        
	public function testJsDatepicker()
	{
		////////////////////////
		// WITH DATEPICKER JS //
		////////////////////////

		$this->Bs->dpLoad = true;
		$result = $this->Bs->js();

		$expected = array(
			array('script' => array('src')), '/script',
			array('script' => array('src')), '/script',
		);

		$this->assertHtml($expected, $result);
	}
 
	public function testMultipleJs()
	{
		/////////////
		// MORE JS //
		/////////////

		$result = $this->Bs->js(array('myJs', 'myOther'));

		$expected = array(
			array('script' => array('src')), '/script',
			array('script' => array('src')), '/script',
			array('script' => array('src' => '/js/myJs.js')), '/script',
			array('script' => array('src' => '/js/myOther.js')), '/script',
		);
		$this->assertHtml($expected, $result);
	}

	public function testClose()
	{
		$result = $this->Bs->close();

		$expected = array('/div');

		$this->assertHtml($expected, $result);
	}

	public function testHeader()
	{
		/////////////////////
		// WITHOUT OPTIONS //
		/////////////////////

		$result = $this->Bs->header();

		$expected = array('<header');

		$this->assertHtml($expected, $result);

		//////////////////
		// WITH OPTIONS //
		//////////////////

		$result = $this->Bs->header(array('id' => 'myId', 'class' => 'classTest'));

		$expected = array(array('header' => array('id' => 'myId', 'class' => 'classTest')));

		$this->assertHtml($expected, $result);
	}

	public function testCloseHeader()
	{
		$result = $this->Bs->closeHeader();

		$expected = ['/header'];

		$this->assertHtml($expected, $result);
	}
    
	public function testContainer()
	{
		///////////////////
		// WITHOUT CLASS //
		///////////////////

		$result = $this->Bs->container();

		$expected = [
			['div' => ['class' => 'container']],
		];

		$this->assertHtml($expected, $result);

		////////////////
		// WITH CLASS //
		////////////////

		$result = $this->Bs->container(['class' => 'classTest']);

		$expected = [
			['div' => ['class' => 'container classTest']],
		];

		$this->assertHtml($expected, $result);
	}
    
	public function testRow()
	{
		///////////////////
		// WITHOUT CLASS //
		///////////////////

		$result = $this->Bs->row();

		$expected = [
			['div' => ['class' => 'row']],
		];

		$this->assertHtml($expected, $result);

		////////////////
		// WITH CLASS //
		////////////////

		$result = $this->Bs->row(['class' => 'classTest']);

		$expected = [
			['div' => ['class' => 'row classTest']],
		];

		$this->assertHtml($expected, $result);
	}

	public function testCol()
	{
		$result = $this->Bs->col('xs12', 'sm3 of1', 'md2 pl2', 'lg4 ph9 of2', ['id' => 'idTest', 'class' => 'classeTest']);

		$expected = [
			['div' => [
				'id'    => 'idTest',
				'class' => 'col-xs-12 col-sm-3 col-sm-offset-1 col-md-2 col-md-pull-2 col-lg-4 col-lg-push-9 col-lg-offset-2 classeTest'],
			],
		];

		$this->assertHtml($expected, $result);

		///////////////////////
		// WITH WRONG PARAMS //
		///////////////////////

		$result = $this->Bs->col('xs12 md-3', 'sm3 or2');

		$expected = [
			['div' => [
				'class' => 'col-xs-12 col-sm-3'],
			],
		];

        $this->assertHtml($expected, $result);
	}
    
	public function testTable()
	{
		$titles = [
			['title' => 'Cell1', 'width' => '50', 'hidden' => ['xs']],
			['title' => 'Cell2', 'width' => '50'],
		];

		$result = $this->Bs->table($titles);

		$expected = [
			['div' => ['class' => 'table-responsive']],
			['table' => ['class' => 'table']],
			'<thead',
			'<tr',
			['th' => ['class' => 'l_50 hidden-xs']],
			'Cell1',
			'/th',
			['th' => ['class' => 'l_50']],
			'Cell2',
			'/th',
			'/tr',
			'/thead',
			'<tbody',
		];

        $this->assertHtml($expected, $result);

		//////////////////
		// WITH OPTIONS //
		//////////////////

		$titles = [
			['title' => 'Cell1', 'width' => '50', 'hidden' => ['xs']],
			['title' => 'Cell2', 'width' => '50'],
		];

		$result = $this->Bs->table($titles, ['hover', 'striped']);

		$expected = [
			['div' => ['class' => 'table-responsive']],
			['table' => ['class' => 'table table-hover table-striped']],
			'<thead',
			'<tr',
			['th' => ['class' => 'l_50 hidden-xs']],
			'Cell1',
			'/th',
			['th' => ['class' => 'l_50']],
			'Cell2',
			'/th',
			'/tr',
			'/thead',
			'<tbody',
		];

        $this->assertHtml($expected, $result);

		/////////////////
		// TABLESORTER //
		/////////////////

		$titles = [
			['title' => 'Cell1', 'width' => '50', 'hidden' => ['xs']],
			['title' => 'Cell2', 'width' => '50'],
		];

		$result = $this->Bs->table($titles, ['hover', 'striped', 'tablesorter']);

		$expected = [
			['div' => ['class' => 'table-responsive']],
			['table' => ['class' => 'table table-hover table-striped tablesorter']],
			'<thead',
			'<tr',
			['th' => ['class' => 'l_50 hidden-xs']],
			'Cell1',
			'/th',
			['th' => ['class' => 'l_50']],
			'Cell2',
			'/th',
			'/tr',
			'/thead',
			'<tbody',
		];

		//////////////////
		// SIMPLE TABLE //
		//////////////////

		$titles = [
			['title' => 'Cell1'],
			['title' => 'Cell2'],
		];

		$result = $this->Bs->table($titles);

		$expected = [
			['div' => ['class' => 'table-responsive']],
			['table' => ['class' => 'table']],
			'<thead',
			'<tr',
			['th' => ['class']],
			'Cell1',
			'/th',
			['th' => ['class']],
			'Cell2',
			'/th',
			'/tr',
			'/thead',
			'<tbody',
		];
	}

	public function testCell()
	{
		////////////////////////////////////////////////
		// CELL WITH SIMPLE TABLE AND WITHOUT ROWLINK //
		////////////////////////////////////////////////

		$titles = array(
			array('title' => 'Cell1'),
			array('title' => 'Cell2')
		);
		$this->Bs->table($titles);

		$result = $this->Bs->cell('Test1', '', false) .
		$this->Bs->cell('Test2', '', false);

		$this->Bs->endTable();
		$expected = array(
			'<tr',
			'<td',
			'Test1',
			'/td',
			'<td',
			'Test2',
			'/td',
			'/tr',
		);
        $this->assertHtml($expected, $result);

		////////////////
		// WITH WIDTH //
		////////////////

		$titles = array(
			array('title' => 'Cell1', 'width' => '50', 'hidden' => array('xs')),
			array('title' => 'Cell2', 'width' => '50'),
		);
		$this->Bs->table($titles);

		$result = $this->Bs->cell('Test1') .
		$this->Bs->cell('Test2');

		$this->Bs->endTable();
		$expected = array(
			'<tr',
			array('td' => array('class' => 'hidden-xs')),
			'Test1',
			'/td',
			'<td',
			'Test2',
			'/td',
			'/tr',
		);
        $this->assertHtml($expected, $result);

		//////////////////
		// WITH OPTIONS //
		//////////////////

		$this->Bs->table($titles);
		$result = $this->Bs->cell('Test1', 'classTest') .
		$this->Bs->cell('Test2', '', false);
		$this->Bs->endTable();

		$expected = array(
			'<tr',
			array('td' => array('class' => 'classTest hidden-xs')),
			'Test1',
			'/td',
			'<td',
			'Test2',
		);

		$this->Bs->table($titles);
		$result = $this->Bs->cell('Test1', 'classTest', false) .
		$this->Bs->cell('Test2', '', false, false);
		$this->Bs->endTable();

		$expected = array(
			'<tr',
			array('td' => array('class' => 'classTest hidden-xs')),
			'Test1',
			'/td',
			'<td',
			'Test2',
		);
		
        $this->assertHtml($expected, $result);

		$this->Bs->table($titles);
		$result = $this->Bs->cell('Test1', 'classTest', false) .
		$this->Bs->cell('Test2', '', false) .
		$this->Bs->cell('Test3', '', false);
		$this->Bs->endTable();

		$expected = array(
			'<tr',
			array('td' => array('class' => 'classTest hidden-xs')),
			'Test1',
			'/td',
			'<td',
			'Test2',
			'/td',
			'/tr',
			'<tr',
			array('td' => array('class' => 'hidden-xs')),
			'Test3',
			'/td'
		);
        $this->assertHtml($expected, $result);

        Router::connect('/:controller/:action/*');
		$this->Bs->table($titles, array(), true);
		$this->Bs->setCellLink(['controller' => 'Urltest', 'action' => 'test']);
		$result = $this->Bs->cell('Test1', 'classTest', true) .
		$this->Bs->cell('Test2', '', false) .
		$this->Bs->cell('Test3', '', false);
		$this->Bs->endTable();

		$expected = array(
			'<tr',
			array('td' => array('class' => 'classTest hidden-xs')),
			array('a'  => array('href' => "/Urltest/test")),
			'Test1',
			'/a',
			'/td',
			array('td' => array('class' => ' rowlink-skip ')),
			'Test2',
			'/td',
			'/tr',
			'<tr',
			array('td' => array('class' => 'hidden-xs')),
			'Test3',
			'/td'
		);
        $this->assertHtml($expected, $result);

		////////////////////////
		// WITHOUT AUTOFORMAT //
		////////////////////////
		$this->Bs->table($titles);
		$result = $this->Bs->cell('Test1', 'classTest', false, false) .
		'</td>' .
		$this->Bs->cell('Test2', '', false, false) .
		'</td>' .
		'</tr>' .
		$this->Bs->cell('Test3', '', false, false) .
		'</td>' .
		$this->Bs->cell('Test4', '', false);
		$this->Bs->endTable();

		$expected = array(
			'<tr',
			array('td' => array('class' => 'classTest hidden-xs')),
			'Test1',
			'/td',
			'<td',
			'Test2',
			'/td',
			'/tr',
			'<tr',
			array('td' => array('class' => 'hidden-xs')),
			'Test3',
			'/td',
			'<td',
			'Test4',
			'/td',
			'/tr'
		);
        $this->assertHtml($expected, $result);
	}
    
	public function testLineColor()
	{
		$titles = array(
			array('title' => 'Cell1', 'width' => '50'),
			array('title' => 'Cell2', 'width' => '50'),
		);
		$this->Bs->table($titles);

		$result = $this->Bs->lineColor('success') .
		$this->Bs->cell('Test1') .
		$this->Bs->cell('Test2') .
		$this->Bs->cell('Test3') .
		$this->Bs->cell('Test4');

		$expected = array(
			array('tr' => array('class' => 'success')),
			'<td',
			'Test1',
			'/td',
			'<td',
			'Test2',
			'/td',
			'/tr',
			'<tr',
			'<td',
			'Test3',
			'/td',
			'<td',
			'Test4',
			'/td',
			'/tr',
		);
        $this->assertHtml($expected, $result);
	}    

	public function testEndTable()
	{
		$result = $this->Bs->endTable();

		$expected = array(
			'/tbody',
			'/table',
			'/div',
		);

        $this->assertHtml($expected, $result);
	}

	public function testAlert()
	{
		/////////////////
		// BASIC ALERT //
		/////////////////

		$result = $this->Bs->alert('myAlert', 'warning');

		$expected = array(
			array('div' => array('class' => 'alert alert-warning')),
			array('button' => array('type' => 'button', 'class' => 'close', 'data-dismiss' => 'alert', 'aria-hidden' => 'true')),
			'&times;',
			'/button',
			'myAlert',
			'/div',
		);

        $this->assertHtml($expected, $result);

		////////////////////////
		// ALERT WITH OPTIONS //
		////////////////////////

		$result = $this->Bs->alert('myAlert', 'warning', array('id' => 'myId', 'class' => 'classTest'));

		$expected = array(
			array('div' => array('id' => 'myId', 'class' => 'alert alert-warning classTest')),
			array('button' => array('type' => 'button', 'class' => 'close', 'data-dismiss' => 'alert', 'aria-hidden' => 'true')),
			'&times;',
			'/button',
			'myAlert',
			'/div',
		);

        $this->assertHtml($expected, $result);
	}

	public function testImage()
	{
		///////////////////
		// WITHOUT CLASS //
		///////////////////

		$result = $this->Bs->image('random.jpg');

		$expected = array(
			array(
				'img' => array(
					'src',
					'class' => 'img-responsive',
					'alt'   => '',
				),
			),
		);

        $this->assertHtml($expected, $result);

		////////////////
		// WITH CLASS //
		////////////////

		$result = $this->Bs->image('random.jpg', array('class' => 'classTest'));

		$expected = array(
			array(
				'img' => array(
					'src',
					'class' => 'img-responsive classTest',
					'alt'   => '',
				),
			),
		);

        $this->assertHtml($expected, $result);
	}

	public function testIcon()
	{
		$result = $this->Bs->icon('user', array('class' => 'classeTest'), array('title' => 'test'));

		$expected = array(
			array('i' => array(
				'title' => 'test',
				'class' => 'fa fa-user fa-classeTest',
			),
			),
			'/i',
		);

        $this->assertHtml($expected, $result);
	}

	public function testBtn()
	{
		//////////////////
		// BASIC BUTTON //
		//////////////////

		Router::connect('/:controller/:action/*');
		$result = $this->Bs->btn('Link', array('controller' => 'pages', 'action' => 'col'));

		$expected = array(
			array('a' => array('class' => 'btn', 'href')), 'Link', '/a',
		);

        $this->assertHtml($expected, $result);

		//////////////////////////////
		// BUTTON WITH MANY OPTIONS //
		//////////////////////////////

		$result = $this->Bs->btn('Link', array('controller' => 'pages', 'action' => 'col'), array('type' => 'primary', 'size' => 'lg', 'class' => 'classTest', 'tag' => 'button'));

		$expected = array(
			array('button' => array('class' => 'btn btn-primary btn-lg classTest')), 'Link', '/button',
		);

        $this->assertHtml($expected, $result);
	}
 
	public function testModal()
	{
		Router::connect('/:controller/:action/*');

		/////////////////
		// BASIC MODAL //
		/////////////////

		$result = $this->Bs->modal('Modal title', 'Modal content');

		$expected = array(
			array('button' => array('class' => 'btn btn-primary btn-lg', 'data-target', 'data-toggle' => 'modal')), 'Voir', '/button',
			array('div' => array('class' => 'modal fade ', 'id', 'tabindex', 'role', 'aria-labelledby', 'aria-hidden')),
			array('div' => array('class' => 'modal-dialog')),
			array('div' => array('class' => 'modal-content')),
			array('div' => array('class' => 'modal-header')),
			array('button' => array('type', 'class' => 'close', 'data-dismiss' => 'modal', 'aria-hidden' => 'true')), '&times;', '/button',
			array('h4' => array('class' => 'modal-title', 'id')), 'Modal title', '/h4',
			'/div',
			array('div' => array('class' => 'modal-body')),
			'<p',
			'Modal content',
			'/p',
			'/div',
			'/div',
			'/div',
			'/div',
		);

		$this->assertHtml($expected, $result);

		//////////////////
		// MORE OPTIONS //
		//////////////////

		$options = array(
			'id'    => 'myId',
			'class' => 'classTest',
		);

		$result = $this->Bs->modal('Modal title', 'Modal content', $options);

		$expected = array(
			array('button' => array('class' => 'btn btn-primary btn-lg', 'data-target' => '#myId', 'data-toggle' => 'modal')), 'Voir', '/button',
			array('div' => array('class' => 'modal fade classTest', 'id' => 'myId', 'tabindex', 'role', 'aria-labelledby', 'aria-hidden')),
			array('div' => array('class' => 'modal-dialog')),
			array('div' => array('class' => 'modal-content')),
			array('div' => array('class' => 'modal-header')),
			array('button' => array('type', 'class' => 'close', 'data-dismiss' => 'modal', 'aria-hidden' => 'true')), '&times;', '/button',
			array('h4' => array('class' => 'modal-title', 'id')), 'Modal title', '/h4',
			'/div',
			array('div' => array('class' => 'modal-body')),
			'<p',
			'Modal content',
			'/p',
			'/div',
			'/div',
			'/div',
			'/div',
		);

		$this->assertHtml($expected, $result);

		////////////////
		// MODAL FORM //
		////////////////

		$options = array(
			'id'    => 'myId',
			'class' => 'classTest',
			'form'  => true,
		);

		$body = $this->BsForm->create('myModel', array('url' => ['controller' => 'myController', 'action' => 'myAction']));
		$body .= $this->BsForm->input('myField');
		$body .= $this->BsForm->submit('Send');
		$body .= $this->BsForm->end();

		$result = $this->Bs->modal('Modal title', $body, $options);

		$expected = [
			['button' => ['class' => 'btn btn-primary btn-lg', 'data-target' => '#myId', 'data-toggle' => 'modal']],
				'Voir',
			'/button',
			['div' => ['class' => 'modal fade classTest', 'id' => 'myId', 'tabindex', 'role', 'aria-labelledby', 'aria-hidden']],
				['div' => ['class' => 'modal-dialog']],
					['div' => ['class' => 'modal-content']],
						['form' => ['action' => '/myController/myAction', 'role' => 'form', 'class' => 'form-horizontal', 'method' => 'post', 'accept-charset']],
							['div' => ['class' => 'modal-header']],
								['button' => ['type', 'class' => 'close', 'data-dismiss' => 'modal', 'aria-hidden' => 'true']], '&times;', '/button',
								['h4' => ['class' => 'modal-title', 'id']], 'Modal title', '/h4',
							'/div',
							['div' => ['class' => 'modal-body']],
								['div' => ['style' => 'display:none;']],
									['input' => ['type' => 'hidden', 'name', 'value']],
								'/div',
								['div' => ['class' => 'form-group']],
									['label' => ['for', 'class' => 'control-label col-md-3']],
										'My Field',
									'/label',
									['div' => ['class' => 'col-md-9']],
										['input' => ['type', 'name', 'class' => 'form-control ', 'id']],
									'/div',
								'/div',
								['div' => ['class' => 'form-group']],
									['div' => ['class' => 'col-md-9 col-md-offset-3']],
										['button' => ['class' => 'btn btn-success', 'type' => 'submit']],
											'Send',
										'/button',
										// ['i' => ['class' => 'fa fa-spinner fa-spin form-submit-wait text-success']],
										// '/i',
									'/div',
								'/div',
							'/div',
						'/form',
					'/div',
				'/div',
			'/div'
		];

		$this->assertHtml($expected, $result);

		///////////////////////////////
		// MODAL WITH CUSTOM BUTTONS //
		///////////////////////////////

		$buttons = array(
			'open' => 'Open',
		);
		$result = $this->Bs->modal('Modal title', 'Modal content', array(), $buttons);

		$expected = array(
			array('button' => array('class' => 'btn btn-primary btn-lg', 'data-target', 'data-toggle' => 'modal')), 'Open', '/button',
			array('div' => array('class' => 'modal fade ', 'id', 'tabindex', 'role', 'aria-labelledby', 'aria-hidden')),
			array('div' => array('class' => 'modal-dialog')),
			array('div' => array('class' => 'modal-content')),
			array('div' => array('class' => 'modal-header')),
			array('button' => array('type', 'class' => 'close', 'data-dismiss' => 'modal', 'aria-hidden' => 'true')), '&times;', '/button',
			array('h4' => array('class' => 'modal-title', 'id')), 'Modal title', '/h4',
			'/div',
			array('div' => array('class' => 'modal-body')),
			'<p',
			'Modal content',
			'/p',
			'/div',
			'/div',
			'/div',
			'/div',
		);

		$this->assertHtml($expected, $result);

		$buttons = array(
			'open'  => array('button' => false, 'name' => 'users'),
			'close' => 'Close',
		);
		$result = $this->Bs->modal('Modal title', 'Modal content', array(), $buttons);

		$expected = array(
			array('i' => array('class' => 'fa fa-users fa-open-modal', 'data-target', 'data-toggle' => 'modal')), '/i',
			array('div' => array('class' => 'modal fade ', 'id', 'tabindex', 'role', 'aria-labelledby', 'aria-hidden')),
			array('div' => array('class' => 'modal-dialog')),
			array('div' => array('class' => 'modal-content')),
			array('div' => array('class' => 'modal-header')),
			array('button' => array('type', 'class' => 'close', 'data-dismiss' => 'modal', 'aria-hidden' => 'true')), '&times;', '/button',
			array('h4' => array('class' => 'modal-title', 'id')), 'Modal title', '/h4',
			'/div',
			array('div' => array('class' => 'modal-body')),
			'<p',
			'Modal content',
			'/p',
			'/div',
			array('div' => array('class' => 'modal-footer')),
			array('button' => array('class' => 'btn btn-link', 'data-dismiss' => 'modal')),
			'Close',
			'/button',
			'/div',
			'/div',
			'/div',
			'/div',
		);

		$this->assertHtml($expected, $result);

		$buttons = array(
			'close' => array('name' => 'Close', 'class' => 'closeClass'),
			'confirm',
		);

		$result = $this->Bs->modal('Modal title', 'Modal content', array(), $buttons);

		$expected = array(
			array('button' => array('class' => 'btn btn-primary btn-lg', 'data-target', 'data-toggle' => 'modal')), 'Voir', '/button',
			array('div' => array('class' => 'modal fade ', 'id', 'tabindex', 'role', 'aria-labelledby', 'aria-hidden')),
			array('div' => array('class' => 'modal-dialog')),
			array('div' => array('class' => 'modal-content')),
			array('div' => array('class' => 'modal-header')),
			array('button' => array('type', 'class' => 'close', 'data-dismiss' => 'modal', 'aria-hidden' => 'true')), '&times;', '/button',
			array('h4' => array('class' => 'modal-title', 'id')), 'Modal title', '/h4',
			'/div',
			array('div' => array('class' => 'modal-body')),
			'<p',
			'Modal content',
			'/p',
			'/div',
			array('div' => array('class' => 'modal-footer')),
			array('button' => array('class' => 'btn closeClass', 'data-dismiss' => 'modal')),
			'Close',
			'/button',
			array('button' => array('class' => 'btn btn-button btn-success')),
			'Confirmer',
			'/button',
			'/div',
			'/div',
			'/div',
			'/div',
		);

		$this->assertHtml($expected, $result);

		//////////////////////////////////////
		// SPECIAL CASE : FORM WITH CONFIRM //
		//////////////////////////////////////

		$options = array(
			'form' => true,
		);
		$buttons = array(
			'confirm' => array('name' => 'Confirm', 'class' => 'confirmClass'),
		);

		$body = $this->BsForm->create('myModel', ['url' => ['controller' => 'myController', 'action' => 'myAction']]);
		$body .= $this->BsForm->input('myField');
		$body .= $this->BsForm->end();

		$result = $this->Bs->modal('Modal title', $body, $options, $buttons);

		$expected = array(
			array('button' => array('class' => 'btn btn-primary btn-lg', 'data-target', 'data-toggle' => 'modal')), 'Voir', '/button',
			array('div' => array('class' => 'modal fade ', 'id', 'tabindex', 'role', 'aria-labelledby', 'aria-hidden')),
			array('div' => array('class' => 'modal-dialog')),
			array('div' => array('class' => 'modal-content')),
			array('form' => array('action' => '/myController/myAction', 'role' => 'form', 'class' => 'form-horizontal', 'method' => 'post', 'accept-charset')),
			array('div' => array('class' => 'modal-header')),
			array('button' => array('type', 'class' => 'close', 'data-dismiss' => 'modal', 'aria-hidden' => 'true')), '&times;', '/button',
			array('h4' => array('class' => 'modal-title', 'id')), 'Modal title', '/h4',
			'/div',
			array('div' => array('class' => 'modal-body')),
			array('div' => array('style' => 'display:none;')),
			array('input' => array('type' => 'hidden', 'name', 'value')),
			'/div',
			array('div' => array('class' => 'form-group')),
			array('label' => array('for', 'class' => 'control-label col-md-3')), 'My Field', '/label',
			array('div' => array('class' => 'col-md-9')),
			array('input' => array('name', 'class' => 'form-control ', 'type', 'id')),
			'/div',
			'/div',
			'/div',
			array('div' => array('class' => 'modal-footer')),
			array('button' => array('class' => 'btn btn-submit confirmClass')),	
				'Confirm',
			'/button',
			'/div',
			'/form',
			'/div',
			'/div',
			'/div',
		);

		$this->assertHtml($expected, $result);
	}

	public function testConfirm()
	{
		Router::connect('/:controller/:action/*');

		///////////////////
		// BASIC CONFIRM //
		///////////////////

		$result = $this->Bs->confirm('Link', array('controller' => 'pages', 'action' => 'col'));

		$expected = array(
			array('button' => array('class' => 'btn btn-success', 'data-target', 'data-toggle' => 'modal')), 'Link', '/button',
			array('div' => array('class' => 'modal fade ', 'id', 'tabindex', 'role', 'aria-labelledby', 'aria-hidden')),
			array('div' => array('class' => 'modal-dialog')),
			array('div' => array('class' => 'modal-content')),
			array('div' => array('class' => 'modal-header')),
			array('button' => array('type', 'class' => 'close', 'data-dismiss' => 'modal', 'aria-hidden' => 'true')), '&times;', '/button',
			array('h4' => array('class' => 'modal-title', 'id')), 'Link', '/h4',
			'/div',
			array('div' => array('class' => 'modal-body')),
			'<p',
			'Voulez-vous vraiment continuer votre action ?',
			'/p',
			'/div',
			array('div' => array('class' => 'modal-footer')),
			array('button' => array('class' => 'btn btn-link', 'data-dismiss' => 'modal')), 'Fermer', '/button',
			array('a' => array('href', 'class' => 'btn btn-success')), 'Link', '/a',
			'/div',
			'/div',
			'/div',
			'/div',
		);

		$this->assertHtml($expected, $result);

		//////////////////////////////////
		// CONFIRM WITH LOTS OF OPTIONS //
		//////////////////////////////////

		$options = array(
			'color'  => 'primary',
			'texte'  => 'my text',
			'button' => 'Send',
			'header' => 'Confirm action',
		);

		$result = $this->Bs->confirm('Link', array('controller' => 'pages', 'action' => 'col'), $options);

		$expected = array(
			array('button' => array('class' => 'btn btn-primary', 'data-target', 'data-toggle' => 'modal')), 'Link', '/button',
			array('div' => array('class' => 'modal fade ', 'id', 'tabindex', 'role', 'aria-labelledby', 'aria-hidden')),
			array('div' => array('class' => 'modal-dialog')),
			array('div' => array('class' => 'modal-content')),
			array('div' => array('class' => 'modal-header')),
			array('button' => array('type', 'class' => 'close', 'data-dismiss' => 'modal', 'aria-hidden' => 'true')), '&times;', '/button',
			array('h4' => array('class' => 'modal-title', 'id')), 'Confirm action', '/h4',
			'/div',
			array('div' => array('class' => 'modal-body')),
			'<p',
			'my text',
			'/p',
			'/div',
			array('div' => array('class' => 'modal-footer')),
			array('button' => array('class' => 'btn btn-link', 'data-dismiss' => 'modal')), 'Fermer', '/button',
			array('a' => array('href', 'class' => 'btn btn-primary')), 'Send', '/a',
			'/div',
			'/div',
			'/div',
			'/div',
		);

		$this->assertHtml($expected, $result);
	}

	public function testLoadCSS()
	{
		$value = 'https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.css';

		$this->View->assign('cssTop', 'Block');
		$this->Bs->loadCSS($value);

		$result = $this->View->fetch('cssTop');

		$this->assertSame('Block<link rel="stylesheet" href="' . $value . '"/>', $result);
	}

	public function testLoadJS()
	{
		$value = 'https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.js';

		$this->View->assign('scriptBottom', 'Block');

		/////////////////
		// SANS OPTION //
		/////////////////

		$this->Bs->loadJS($value);

		$result = $this->View->fetch('scriptBottom');
		$this->assertSame('Block<script src="' . $value . '"></script>', $result);

		/////////////////
		// SANS OPTION //
		/////////////////
		$this->View->assign('scriptBottom', '');

		$this->Bs->loadJS('console.log()', true, array('block' => 'scriptBottom'));

		$result = $this->View->fetch('scriptBottom');
		$this->assertSame('<script>
//<![CDATA[
console.log()
//]]>
</script>', $result);
	}
}