<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Produits Model
 *
 * @method \App\Model\Entity\Produit get($primaryKey, $options = [])
 * @method \App\Model\Entity\Produit newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Produit[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Produit|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Produit|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Produit patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Produit[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Produit findOrCreate($search, callable $callback = null, $options = [])
 */
class ProduitsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('produits');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('designation_produit')
            ->maxLength('designation_produit', 255)
            ->allowEmptyString('designation_produit');

        $validator
            ->scalar('marque')
            ->maxLength('marque', 255)
            ->allowEmptyString('marque');

        $validator
            ->scalar('code_ean')
            ->maxLength('code_ean', 255)
            ->allowEmptyString('code_ean');

        $validator
            ->scalar('numero_pdv')
            ->maxLength('numero_pdv', 255)
            ->allowEmptyString('numero_pdv');

        $validator
            ->scalar('societe')
            ->maxLength('societe', 255)
            ->allowEmptyString('societe');

        $validator
            ->scalar('adresse')
            ->maxLength('adresse', 255)
            ->allowEmptyString('adresse');

        $validator
            ->scalar('code_postal')
            ->maxLength('code_postal', 255)
            ->allowEmptyString('code_postal');

        $validator
            ->scalar('ville')
            ->maxLength('ville', 255)
            ->allowEmptyString('ville');

        return $validator;
    }
}
