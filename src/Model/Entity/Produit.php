<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Produit Entity
 *
 * @property int $id
 * @property string|null $designation_produit
 * @property string|null $marque
 * @property string|null $code_ean
 * @property string|null $numero_pdv
 * @property string|null $societe
 * @property string|null $adresse
 * @property string|null $code_postal
 * @property string|null $ville
 */
class Produit extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'designation_produit' => true,
        'marque' => true,
        'code_ean' => true,
        'numero_pdv' => true,
        'societe' => true,
        'adresse' => true,
        'code_postal' => true,
        'ville' => true,
        'telephone' => true,
    ];
}
