<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Utility\Inflector;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController
{
    public function display()
    {

    }
    /**
     * Page d'accueil
     */
    public function index()
    {

        $this->loadModel('Produits');
        $produits = $this->Produits->find()->select(['designation_produit', 'code_ean'])->distinct(['designation_produit', 'code_ean']);

        $count = [];

        // Calcul le nombre de lignes de chaque produit
        foreach($produits as $produit) {
            $count[$produit->designation_produit] = $this->Produits->findByDesignationProduit($produit->designation_produit)->count();
        }

        $this->set(compact('produits', 'count'));
    }

    /**
     * Vide la table produits puis enregistre toutes les lignes du fichier CSV
     */
    public function uploadCsv()
    {
        // Si le fichier existe
        if(isset($_FILES['csv_file'])) {
            if(pathinfo($_FILES['csv_file']['name'], PATHINFO_EXTENSION) != 'csv') {
                $this->Flash->error(__('Veuillez sélectionner un fichier au format CSV.'));

                return $this->redirect($this->referer());
            }

            // Vide les données de la table produits
            $this->emptyData();

            // Récupère le fichier CSV en tableau
            // $data = array_map('str_getcsv', file($_FILES['csv_file']['tmp_name']));
            $tableauDesLignesDuFichierCSV = array();
            // debug($_FILES['csv_file']);die;
            $test = fopen($_FILES['csv_file']['tmp_name'], 'r');
            $row = 0;
    		//Pour chaque ligne on met les données séparées par une ',' dans un tableau tabData
            while (($tabData = fgetcsv($test, 10000, ";")) !== FALSE) {
                //On compte le nb de données par ligne
                $nombreDeData = count($tabData);
                //On insère les données dans un tableau trié par ligne
                for ($numeroData=0; $numeroData < $nombreDeData; $numeroData++) {
                    $tableauDesLignesDuFichierCSV['Ligne'.$row][] = trim($tabData[$numeroData]);
                }
                $row++;
            }
            // debug($tableauDesLignesDuFichierCSV);die;
            // Supprime la première ligne du tableau de données (nom des colonnes)
            array_shift($tableauDesLignesDuFichierCSV);

            // Insère toutes les données (PROCESS A MODIFIER A L'AVENIR)
            foreach($tableauDesLignesDuFichierCSV as $keyRow => $row) {
                if($keyRow == 'Ligne0') {
                    continue;
                }
                $produit = $this->Produits->newEntity([
                    'designation_produit' => preg_replace('/[\x00-\x1F\x7F-\xFF]/', '', $row[0]),
                    // 'marque' => preg_replace('/[\x00-\x1F\x7F-\xFF]/', '', $row[1]),
                    'code_ean' => preg_replace('/[\x00-\x1F\x7F-\xFF]/', '', $row[1]),
                    // 'numero_pdv' => preg_replace('/[\x00-\x1F\x7F-\xFF]/', '', $row[3]),
                    'societe' => preg_replace('/[\x00-\x1F\x7F-\xFF]/', '', $row[2]),
                    'adresse' => preg_replace('/[\x00-\x1F\x7F-\xFF]/', '', $row[5]),
                    'code_postal' => preg_replace('/[\x00-\x1F\x7F-\xFF]/', '', $row[6]),
                    'ville' => preg_replace('/[\x00-\x1F\x7F-\xFF]/', '', $row[7]),
                    // 'telephone' => preg_replace('/[\x00-\x1F\x7F-\xFF]/', '', $row[6]),
                ]);
                if(!$this->Produits->save($produit)) {
                    $this->Flash->error(__('Erreur lors de l\'importation d\'un produit.'));

                    return $this->redirect($this->referer());
                }
            }
        }

        $this->Flash->success(__('Données correctement importées.'));

        return $this->redirect($this->referer());
    }

    /**
     * Exporte en CSV toutes les lignes d'un produit
     */
    public function exportCsv($designation_produit = null)
    {
        // $produits = $this->Produits->findByDesignationProduit($designation_produit);
        $this->loadModel('Produits');
        $produits = $this->Produits->findByCodeEan($designation_produit);
        // Création du fichier
        $fp = fopen(WWW_ROOT . 'files' . DS . Inflector::slug($designation_produit) . '.csv', 'w+');

        // Vide le fichier
        ftruncate($fp, 0);

        // UTF8
        fprintf($fp, chr(0xEF).chr(0xBB).chr(0xBF));

        // Création des colonnes
        fputcsv($fp, [
            'nom_produit_fichier_excel',
            'designation_pdv',
            'adresse_pdv',
            'code_postal_pdv',
            'ville_pdv',
            // 'tel_pdv',
        ], ';');

        // Récupération des données

        // $produits = $this->Produits->findByDesignationProduit($designation_produit);
        foreach($produits as $produit) {

            if(strpos($produit->societe,'-') !== false){
                $explodeSociete = explode(' - ', $produit->societe);
                $societe = $explodeSociete[0];
            } else {
                $societe = $produit->societe;
            }

            // $societe = 'Intermarche '.$produit->societe;

            fputcsv($fp, [
                $produit->designation_produit,
                // $produit->societe,
                $societe,
                $produit->adresse,
                $produit->code_postal,
                $produit->ville,
                $produit->telephone
            ], ';');
        }

        // Télécharge le fichier CSV
        $this->response->file(WWW_ROOT . 'files' . DS . Inflector::slug($designation_produit) . '.csv', ['download' => true]);

        return $this->response;
    }

    /**
     * Vide toutes les données de la table produits et supprimer les fichiers CSV téléchargés
     */
    public function emptyData($redirect = false)
    {
        // Supprime les fichiers CSV téléchargés
        array_map('unlink', glob(APP . 'files/*'));

        $this->loadModel('Produits');
        $produits = $this->Produits->find();

        foreach($produits as $produit) {
            $this->Produits->delete($produit);
        }

        if($redirect) {
            $this->Flash->success(__('Données correctement supprimées.'));

            return $this->redirect($this->referer());
        }
    }
}
